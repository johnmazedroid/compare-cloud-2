const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    index: path.resolve(__dirname, 'src', 'index.js'),
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.js(x)?$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /.(png|jpg|ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]',
          },
        },
      },
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
    ],
  },
  resolve: {
    extensions: ['.mjs', '.js', '.json', '.jsx', '.css'],
    alias: {
      '@maze/components': path.resolve(__dirname, './src/components'),
      '@maze/shared-components': path.resolve(
        __dirname,
        './src/components/shared'
      ),
      '@maze/services': path.resolve(__dirname, './src/services'),
      '@maze/http-get-services': path.resolve(
        __dirname,
        './src/services/http/get'
      ),
      '@maze/http-post-services': path.resolve(
        __dirname,
        './src/services/http/post'
      ),
      '@maze/utilities': path.resolve(__dirname, './src/utilities'),
      '@maze/constants': path.resolve(__dirname, './src/constants'),
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'public', 'index.html'),
      favicon: './public/favicon.ico',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: 'src/.htaccess',
          to: '.htaccess',
          toType: 'file',
        },
      ],
    }),
  ],
};
