const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

module.exports = merge(common, {
  mode: 'development',
  target: 'web',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    historyApiFallback: true,
    open: true,
    watchContentBase: true,
    port: 8080,
    hot: true,
    proxy: {
      '/cloud-api': {
        target: 'http://localhost:' + (process.env.port || 3333),
        secure: false,
      },
    },
  },
});
