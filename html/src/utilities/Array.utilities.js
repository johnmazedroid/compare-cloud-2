export const arrayIntersect = (arrayIn, identifierArray) => {
  const arrayOut = arrayIn
    .filter((item) =>
      identifierArray.every((i) => item.cloudProviderIdList.includes(i))
    )
    .map((val) => ({ label: val.name, value: val.name }));

  return arrayOut;
};
