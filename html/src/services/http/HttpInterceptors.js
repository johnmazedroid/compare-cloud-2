import axios from 'axios';

// Proxy Configurations
const proxyDev = {
  baseURL: 'https://compare-cloud-api.com',
  proxy: {
    host: 'compare-cloud-api.com',
    port: 443,
    protocol: 'https:',
  },
};

const proxyConfig = proxyDev;
const axiosApi = axios.create({ ...proxyConfig });

// Request interceptor for API calls
axiosApi.interceptors.request.use(
  async (config) => {
    //   const value = await redisClient.get(rediskey)
    //   const keys = JSON.parse(value)
    config.headers = {
      // 'Authorization': `Bearer ${keys.access_token}`,
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// Response interceptor for API calls
axiosApi.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if (error.response.status === 403 && !originalRequest._retry) {
      originalRequest._retry = true;
      //   const access_token = await refreshAccessToken();
      //   axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
      return axiosApi(originalRequest);
    }
    return Promise.reject(error);
  }
);

export const axiosApiInstance = axiosApi;
