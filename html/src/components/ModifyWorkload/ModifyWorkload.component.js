import React, { useState, useEffect, useRef } from 'react';
import { Row, Col, Form } from 'react-bootstrap';

// Services
import ModifyWorkloadItemsService from './services/ModifyWorkload.items.services';

// Components
import ListItems from '@maze/shared-components/ListItems/ListItems.component';
import SectionWorkload from './SectionWorkload/SectionWorkload.component';
// import TableGrid from './../shared/TableGrid/TableGrid.component';
import SectionSelectedVM from './SectionSelectedVM/SectionSelectedVM.component';
import SectionAlternateVM from './SectionAlternateVM/SectionAlternateVM.component';

// SCSS
import './ModifyWorkload.component.scss';

const ModifyWorkload = (props) => {
  const { cloudProviders, definedData, getOptimizedData } = props;
  const modifyWorkloadFormRef = useRef();
  const [stateCloudProviders, setStateCloudProviders] = useState([]);
  const [stateMenuItems, setStateMenuItems] = useState([]);
  const [stateWorkload, setStateWorkload] = useState([]);
  const [stateSelectedLeftSection, setStateSelectedLeftSection] = useState([]);
  const [stateFilterDTO, setStateFilterDTO] = useState([]);
  const [stateComputeResourceData, setStateComputeResourceData] = useState([]);

  const [stateSelectedVMData, setStateSelectedVMData] = useState([]);
  const [stateAlternateVMData, setStateAlternateVMData] = useState([]);

  useEffect(() => {
    let unmounted = false;
    const { workload } = definedData;

    // Setter methods
    ModifyWorkloadItemsService.setCloudProviders(cloudProviders);
    ModifyWorkloadItemsService.setWorkloadData(workload);

    const selectedCloudProviders =
      ModifyWorkloadItemsService.getSelectedCloudProviders();
    setStateCloudProviders(cloudProviders);
    setStateWorkload(workload);

    if (selectedCloudProviders.length > 0) {
      const menuItems = ModifyWorkloadItemsService.getMenuItems();
      setStateSelectedLeftSection({
        selectedItem: menuItems[0].name,
        selectedSection: menuItems[0].children[0].name,
      });
      setStateMenuItems(menuItems);
    }

    const transformedComputeResourceData =
      ModifyWorkloadItemsService.transformComputeResourceDataForBackend();

    const computeResourcePayload =
      ModifyWorkloadItemsService.getComputeResourcePayload(
        transformedComputeResourceData
      );

    setStateFilterDTO(computeResourcePayload);

    /**
     * Load compute resource data
     */
    ModifyWorkloadItemsService.loadItems$(computeResourcePayload);

    /**
     * All VMs Instance
     */
    const selectedVMData$ = ModifyWorkloadItemsService.getVMInstances$();
    selectedVMData$.then((response) => {
      if (!unmounted) {
        if (response) {
          setStateSelectedVMData(response.selectedVMData); // Selected VM Instances
          setStateAlternateVMData(response.alternateVMData); // Alternate VM Instances
        }
      }
    });

    return () => {
      unmounted = true;
    };
  }, [cloudProviders, definedData]);

  const onSelectMenuItem = (selectedItem) => {
    setStateSelectedLeftSection(selectedItem);
  };

  // const getAlternateVMOptimizedValues = (optimizedValue) => {
  //   getOptimizedData(optimizedValue);
  // };

  return (
    <>
      <div className={'modify-workload-outer-box'}>
        <Row className={'m-0 h-100'}>
          <Col xs={3} className={'p-0 m-0 border-right'}>
            <ListItems
              items={stateMenuItems}
              {...{ onSelectMenuItem }}
            ></ListItems>
          </Col>
          <Col xs={9} className={'py-3'}>
            <Form ref={modifyWorkloadFormRef}>
              <SectionWorkload
                data={stateWorkload}
                cloudProviders={stateCloudProviders}
                selectedLeftSection={stateSelectedLeftSection}
              ></SectionWorkload>

              <SectionSelectedVM data={stateSelectedVMData} />

              <SectionAlternateVM data={stateAlternateVMData} />

              {/*<SectionAlternateVM
                selectedLeftSection={stateSelectedLeftSection}
                filterDTO={stateFilterDTO}
                workload={stateWorkload}
                data={stateComputeResourceData}
                getAlternateVMOptimizedValues={getAlternateVMOptimizedValues}
              /> */}
            </Form>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default ModifyWorkload;
