import React, { useEffect, useState, useRef } from 'react';
import { Row, Col, Form } from 'react-bootstrap';

// For Translation
import i18next from 'i18next';

// SCSS
import './SectionWorkload.component.scss';

const SectionWorkload = (props) => {
  const { data } = props;
  const [stateWorkload, setStateWorkload] = useState([]);
  const [stateSelectedWorkloadData, setStateSelectedWorkloadData] = useState(
    {}
  );
  const [stateWorkloadItems, setStateWorkloadItems] = useState([]);
  const [stateDefaultWorkloadName, setStateDefaultWorkloadName] = useState('');

  useEffect(() => {
    const [workload] = data;
    if (workload) {
      setStateWorkload(workload);
      const selectedWorkloadName = workload?.computeResource.length
        ? workload?.computeResource[0].workloadId
        : ''; // For default option
      setStateDefaultWorkloadName(selectedWorkloadName);
      const workloadNames = getWorkloadNames(workload);
      setStateWorkloadItems(workloadNames);

      const selectedWorkloadData =
        getWorkLoadDataByName(workload, selectedWorkloadName) || {};
      setStateSelectedWorkloadData(selectedWorkloadData);
    }
  }, [data]);

  const getWorkLoadDataByName = (workloadData, workloadId) => {
    const { computeResource } = workloadData;
    return computeResource.find(
      (computeResource) => computeResource.workloadId === workloadId
    );
  };

  const getWorkloadNames = (workloadData) => {
    const { computeResource } = workloadData;

    return computeResource.map((cr) => ({
      label: cr.workloadId,
      value: cr.workloadId,
    }));
  };

  const onWorkloadChange = (event) => {
    const selectedWorkloadName = event.target.value;
    const selectedWorkloadData = getWorkLoadDataByName(
      stateWorkload,
      selectedWorkloadName
    );
    setStateSelectedWorkloadData(selectedWorkloadData);
  };

  return (
    <>
      {stateWorkload && (
        <>
          <div className={'pb-3 border-bottom'}>
            <Form.Group
              controlId="formTCOTermInYears"
              className={'workload-details'}
            >
              <Form.Label className={'text-black-50'}>
                {i18next.t('labelTCOTermYear')}
              </Form.Label>
              <Form.Control
                size={'sm'}
                type="number"
                placeholder={i18next.t('labelTCOTermYear')}
                className={'d-inline w-auto'}
                min={0}
                max={5}
                defaultValue={stateWorkload.tcoTerm}
              />
              {/* <Form.Text className="text-muted">
                {i18next.t("labelTCOTermYear")}
              </Form.Text> */}
            </Form.Group>
            <Form.Group controlId="formWorkload" className={'workload-details'}>
              <Form.Label className={'text-black-50'}>
                {i18next.t('labelWorkload')}
              </Form.Label>
              <Form.Control
                size={'sm'}
                as="select"
                custom
                className={'d-inline w-auto'}
                defaultValue={stateDefaultWorkloadName}
                onChange={onWorkloadChange}
              >
                {stateWorkloadItems.map((wn, wnIndex) => {
                  return <option key={wnIndex}>{wn.label}</option>;
                })}
              </Form.Control>
              {/* <Form.Text className="text-muted">
                {i18next.t("labelWorkload")}
              </Form.Text> */}
            </Form.Group>
            <Row className={'workload-details'}>
              <Col md={6}>
                <div>
                  <label className={'text-black-50'}>
                    {i18next.t('region')}
                  </label>
                  <span>{stateSelectedWorkloadData.region}</span>
                </div>
              </Col>
              <Col md={6}>
                <div>
                  <label className={'text-black-50'}>
                    {i18next.t('instanceType')}
                  </label>
                  <span>{stateSelectedWorkloadData.instanceType}</span>
                </div>
              </Col>
            </Row>
            <Row className={'workload-details'}>
              <Col md={6}>
                <div>
                  <label className={'text-black-50'}>
                    {i18next.t('noOfServers')}
                  </label>
                  <span>{stateSelectedWorkloadData.noOfServers}</span>
                </div>
              </Col>
              <Col md={6}>
                <div>
                  <label className={'text-black-50'}>
                    {i18next.t('instanceFamily')}
                  </label>
                  <span>{stateSelectedWorkloadData.instanceFamily}</span>
                </div>
              </Col>
            </Row>
            <Row className={'workload-details'}>
              <Col md={6}>
                <div>
                  <label className={'mb-0 text-black-50'}>
                    {i18next.t('pricingOption')}
                  </label>
                  <span>
                    {stateSelectedWorkloadData.payAsYouGo
                      ? 'On Demand'
                      : 'Reserved'}
                  </span>
                </div>
              </Col>
              <Col md={6}>
                <div>
                  <label className={'mb-0 text-black-50'}>
                    {i18next.t('operatingSystem')}
                  </label>
                  <span>{stateSelectedWorkloadData.operatingSystemFamily}</span>
                </div>
              </Col>
            </Row>
          </div>
        </>
      )}
    </>
  );
};

export default SectionWorkload;

export const usePreviousSection = (previousValue) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = previousValue;
  });
  return ref.current;
};
