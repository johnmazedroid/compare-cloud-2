import React, { useEffect, useState } from 'react';

// components
import TableGrid from './../../shared/TableGrid/TableGrid.component';

// Language
import i18next from 'i18next';

const SectionSelectedVM = (props) => {
  const { data } = props;
  const [stateSelectedVMData, setStateSelectedVMData] = useState([]);
  const columnDef = [
    {
      headerName: i18next.t('instanceName'),
      field: 'instanceName',
      headerTooltip: i18next.t('instanceName'),
      tooltipField: 'instanceName',
    },
    {
      headerName: i18next.t('noOfServers'),
      field: 'noOfServers',
      headerTooltip: i18next.t('noOfServers'),
    },
    {
      headerName: i18next.t('cost'),
      field: 'cost',
      headerTooltip: i18next.t('cost'),
    },
  ];

  useEffect(() => {
    setStateSelectedVMData(data);
  }, [data]);

  return (
    <>
      <div className={'border-bottom'}>
        <TableGrid
          title={i18next.t('selectedVMInstances')}
          columnDef={columnDef}
          data={stateSelectedVMData}
        ></TableGrid>
      </div>
    </>
  );
};

export default SectionSelectedVM;
