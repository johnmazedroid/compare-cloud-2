import React, { useState, useEffect } from 'react';

// components
import TableGrid from './../../shared/TableGrid/TableGrid.component';

// Language
import i18next from 'i18next';

const SectionAlternateVM = (props) => {
  const { data } = props;
  const [stateSelectedVMData, setStateSelectedVMData] = useState([]);

  const columnDef = [
    {
      headerName: i18next.t('instanceName'),
      field: 'instanceName',
      headerTooltip: i18next.t('instanceName'),
      tooltipField: 'instanceName',
    },
    {
      headerName: i18next.t('vCPU'),
      field: 'vCPU',
      headerTooltip: i18next.t('vCPU'),
    },
    {
      headerName: i18next.t('memory'),
      field: 'memory',
      headerTooltip: i18next.t('memory'),
    },
    {
      headerName: i18next.t('cost'),
      field: 'cost',
      headerTooltip: i18next.t('cost'),
    },
  ];

  useEffect(() => {
    setStateSelectedVMData(data);
  }, [data]);

  return (
    <>
      <TableGrid
        title={i18next.t('alternateVMOptions')}
        columnDef={columnDef}
        data={stateSelectedVMData}
      ></TableGrid>
    </>
  );
};

export default SectionAlternateVM;
