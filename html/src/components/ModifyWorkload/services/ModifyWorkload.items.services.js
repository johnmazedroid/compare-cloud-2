import { errorMessage } from '@maze/utilities/Error.utilities';
import { toast } from 'react-toastify';

import i18next from 'i18next';

// Constants
import { Condition, CustomAttributes } from '@maze/constants';

// services
import { postComputeResourceHTTPService } from '@maze/http-post-services/compute-resource-http.services';
import ModifyWorkloadTransformService from './ModifyWorkload.transform.service';

const ModifyWorkloadItemsService = {
  filteredCloudProviders: [],
  workloadData: [],
  cloudProviders: [],
  computeResourceData$: Promise,

  setCloudProviders: (cloudProviders) => {
    ModifyWorkloadItemsService.cloudProviders = cloudProviders;
  },

  getCloudProviders: () => ModifyWorkloadItemsService.cloudProviders,

  setFilteredCloudProviders: (cloudProviders) => {
    ModifyWorkloadItemsService.filteredCloudProviders = cloudProviders;
  },

  getFilteredCloudProviders: () =>
    ModifyWorkloadItemsService.filteredCloudProviders,

  setWorkloadData: (workloadData) => {
    ModifyWorkloadItemsService.workloadData = workloadData;
  },

  getWorkloadData: () => ModifyWorkloadItemsService.workloadData,

  setComputeResourceData: (data) => {
    ModifyWorkloadItemsService.computeResourceData$ = data;
  },

  getComputeResourceData$: () =>
    ModifyWorkloadItemsService.computeResourceData$,

  getSelectedCloudProviders: () => {
    const workload = ModifyWorkloadItemsService.getWorkloadData();
    const cloudProviders = ModifyWorkloadItemsService.getCloudProviders();
    const selectedCloudProviders = cloudProviders
      .filter((cp) => workload[0].cloudProviders.includes(cp.cloudProviderId))
      .map((cp) => ({ name: cp.name, value: cp.cloudProviderId }));

    ModifyWorkloadItemsService.setFilteredCloudProviders(
      selectedCloudProviders
    );

    return selectedCloudProviders;
  },

  loadItems$: (computeResourcePayload) => {
    const loadItems$ = postComputeResourceHTTPService(computeResourcePayload);

    const computeResourceData$ = loadItems$
      .then((response) => {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            resolve(response);
          }, 1);
        });
      })
      .catch((error) => toast.error(`${errorMessage(error)}`));

    ModifyWorkloadItemsService.setComputeResourceData(computeResourceData$);
  },

  getVMInstances$: () => {
    const computeResourceData$ =
      ModifyWorkloadItemsService.getComputeResourceData$();

    return computeResourceData$.then((response) => {
      return new Promise(function (resolve, reject) {
        setTimeout(function () {
          const transformedSelectedVMData =
            ModifyWorkloadTransformService.transformSelectedVMInstances(
              response
            );

          const transformedAlternateVMData =
            ModifyWorkloadTransformService.transformAlternateVMInstances(
              response
            );

          const transformedResponse = {
            selectedVMData: transformedSelectedVMData,
            alternateVMData: transformedAlternateVMData,
          };
          resolve(transformedResponse);
        }, 1);
      });
    });
  },

  getMenuItems: () => {
    const cloudProviders =
      ModifyWorkloadItemsService.getFilteredCloudProviders();
    const transformedMenuItems = cloudProviders.map((item) => {
      return {
        name: item.name,
        value: item.value,
        className: 'parent',
        children: [
          {
            name: i18next.t('headerComputeResources'),
            className: 'child',
          },
        ],
      };
    });

    return transformedMenuItems;
  },

  transformComputeResourceDataForBackend: () => {
    const workload = ModifyWorkloadItemsService.getWorkloadData();
    const transformDefinedWorkload = workload.map((wl) => {
      return wl.computeResource.map((cp) => {
        return {
          ...cp,
          tcoTerm: wl.tcoTerm.toString(),
          cloudProvider:
            ModifyWorkloadItemsService.getFilteredCloudProviders().map(
              (cp) => cp.name
            ),
          leaseTerm: cp.payAsYouGo
            ? CustomAttributes.ON_DEMAND
            : CustomAttributes.RESERVED,
          usagePerMonth: cp.payAsYouGo ? '730' : cp.usagePerMonth,
          payAsYouGo: cp.payAsYouGo.toString(),
        };
      });
    })[0];

    return transformDefinedWorkload;
  },

  getComputeResourcePayload: (transformedComputeResourceData) => {
    const payload = transformedComputeResourceData.map((twl) => {
      const workloadKeys = Object.keys(twl).filter(
        (key) => !['payAsYouGo'].includes(key)
      );

      return {
        workload: workloadKeys.map((wlKey) => {
          const value = Array.isArray(twl[wlKey]) ? twl[wlKey] : [twl[wlKey]];
          const searchKeyword =
            ModifyWorkloadItemsService.getSearchKeyWord(wlKey);
          return {
            key: wlKey,
            value,
            search: searchKeyword
              ? searchKeyword
              : value.length > 1
              ? Condition.CONTAINS
              : Condition.EQUALITY,
          };
        }),
      };
    });

    return payload;
  },

  getSearchKeyWord: (searchInput) => {
    const searchKey = {
      virtualCpu: Condition.GREATER_THAN,
      memory: Condition.GREATER_THAN,
    };
    return searchKey[searchInput] ?? false;
  },
};

export default ModifyWorkloadItemsService;
