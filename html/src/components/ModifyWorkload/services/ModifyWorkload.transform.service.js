import { Constants } from '@maze/constants';

const ModifyWorkloadTransformService = {
  transformSelectedVMInstances: (data) => {
    return data
      .filter((item) => item.choice === Constants.SELECTED)
      .map((item) => {
        return {
          instanceName: item?.serverAttributes?.instance?.instanceName,
          noOfServers: ModifyWorkloadTransformService.getValuesFromWorkload(
            item,
            'noOfServers'
          ),
          cost: item?.totalPrice,
        };
      });
  },

  transformAlternateVMInstances: (data) => {
    return data
      .filter((item) => item.choice === Constants.OPTIONAL)
      .map((item) => {
        return {
          instanceName: item?.serverAttributes?.instance?.instanceName,
          vCPU: item?.serverAttributes?.virtualCpu,
          memory: item?.serverAttributes?.memory,
          cost: item?.totalPrice,
        };
      });
  },

  getValuesFromWorkload: (item, key) => {
    return item?.computeResourceInputVO?.workload?.find(
      (workload) => workload?.key === key
    ).value[0];
  },
};

export default ModifyWorkloadTransformService;
