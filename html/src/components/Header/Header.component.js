import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// For Translation
import i18next from 'i18next';

import logo from '../../assets/logo.png';

const Header = () => {
    return (
        <Navbar collapseOnSelect expand="lg" bg="white" variant="light" fixed="top">
            <div className={'container'}>
                <Navbar.Brand as={Link} to="/">
                    {/* {i18next.t('logoHeader').toUpperCase()} */}
                    <img
                        src={logo}
                        className="d-inline-block align-top"
                        alt={i18next.t('logoHeader').toUpperCase()}
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/compare">
                            {i18next.t('common:compare')}
                        </Nav.Link>
                        <Nav.Link as={Link} to="/services">
                            {i18next.t('common:services')}
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </div>
        </Navbar>
    )
};

export default Header;