import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import { Container, Row, Col, Form } from 'react-bootstrap';

// Language
import i18next from 'i18next';

import './TableGrid.component.scss';

const TableGrid = (props) => {
  const { data, columnDef, title } = props;
  const [gridApi, setGridApi] = useState(null);
  const [stateRowData, setStateRowData] = useState([]);
  const [stateColumnDef, setStateColumnDef] = useState([]);

  useEffect(() => {
    setStateRowData(data);
    setStateColumnDef(columnDef);
  }, [data, columnDef]);

  const onGridReady = (params) => {
    setGridApi(params.api);
  };

  const onFirstDataRendered = (params) => {
    params.api.sizeColumnsToFit();
  };

  const handleQuickFilter = (event) => {
    gridApi.setQuickFilter(event.target.value);
  };

  return (
    <>
      <div className="mt-3 mb-3">
        <div className="mt-2 mb-2">
          <Container className={'p-0'}>
            <Row className={'m-0 p-0'}>
              <Col className={'m-0 p-0'} md={6}>
                <h5>{title}</h5>
              </Col>
              <Col className={'text-md-right m-0 p-0'} md={6}>
                <Form.Control
                  size={'sm'}
                  type="text"
                  placeholder={i18next.t('common:search')}
                  onChange={handleQuickFilter}
                />
              </Col>
            </Row>
          </Container>
        </div>
        <div className={'table-grid-outer'}>
          <div className={'table-grid-outer-inner'}>
            <div className="ag-theme-alpine">
              <AgGridReact
                style={{ height: '100%', width: '100%' }}
                defaultColDef={{ resizable: true }}
                rowData={stateRowData}
                enableBrowserTooltips={true}
                onFirstDataRendered={onFirstDataRendered}
                rowHeight={30}
                headerHeight={30}
                onGridReady={onGridReady}
              >
                {stateColumnDef.map((columns, index) => (
                  <AgGridColumn
                    {...columns}
                    sortable={true}
                    key={index}
                  ></AgGridColumn>
                ))}
              </AgGridReact>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

TableGrid.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array,
  columnDef: PropTypes.arrayOf(
    PropTypes.exact({
      headerName: PropTypes.string.isRequired,
      field: PropTypes.string.isRequired,
      headerTooltip: PropTypes.string,
      tooltipField: PropTypes.string,
    })
  ),
};

export default TableGrid;
