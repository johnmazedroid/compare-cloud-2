import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';
import {
  Container,
  Row,
  Col,
  Accordion,
  Card,
  Button,
  Table,
} from 'react-bootstrap';

// Language
import i18next from 'i18next';

const CompareOptimize = (props) => {
  const { cloudProviders, definedData, optimizedData } = props;

  const [stateSelectedCloudProviders, setStateSelectedCloudProviders] =
    useState([]);

  const [stateOptimizedData, setStateOptimizedData] = useState([]);

  useEffect(() => {
    const { workload } = definedData;
    const selectedCloudProviders = cloudProviders.filter((cp) =>
      workload[0].cloudProviders.includes(cp.cloudProviderId)
    );
    setStateSelectedCloudProviders(selectedCloudProviders);
    setStateOptimizedData(optimizedData);
  }, [cloudProviders, definedData, optimizedData]);

  return (
    <>
      <Container>
        <Row>
          {stateSelectedCloudProviders.length &&
            stateSelectedCloudProviders.map((cp, cpIndex) => {
              return (
                <>
                  <Col className={'p-0'} key={`Col${cpIndex}`}>
                    <Accordion defaultActiveKey="0" key={`Acc${cpIndex}`}>
                      <Card className={'rounded-0'} key={`Card${cpIndex}`}>
                        <Card.Header
                          className={'p-0'}
                          key={`CardHeader${cpIndex}`}
                        >
                          <Accordion.Toggle
                            as={Button}
                            variant="link"
                            eventKey="0"
                            className={'text-decoration-none text-body'}
                            key={`AccToggle${cpIndex}`}
                          >
                            <div className={'text-body'} key={`Div${cpIndex}`}>
                              <span className={'font-weight-bold'}>
                                {cp.name}
                              </span>
                              <span className={'ml-3 text-muted font-italic'}>
                                {`${i18next.t('computeCost')} ${
                                  stateOptimizedData[0]?.totalPrice
                                }`}
                              </span>
                            </div>
                          </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse
                          eventKey="0"
                          key={`AccCollapse${cpIndex}`}
                        >
                          <Card.Body
                            className={'p-0'}
                            key={`CadBody${cpIndex}`}
                          >
                            <Table
                              striped
                              hover
                              size={'sm'}
                              key={`table${cpIndex}`}
                            >
                              <thead>
                                <tr>
                                  <th>{i18next.t('instanceName')}</th>
                                  <th>{i18next.t('vCPU')}</th>
                                  <th>{i18next.t('memory')}</th>
                                  <th>{i18next.t('cost')}</th>
                                </tr>
                              </thead>
                              <tbody>
                                {stateOptimizedData.length === 0 && (
                                  <tr>
                                    <td colSpan={6}>
                                      {i18next.t('common:noDataFound')}
                                    </td>
                                  </tr>
                                )}
                                {stateOptimizedData &&
                                  stateOptimizedData.length > 0 &&
                                  stateOptimizedData
                                    .filter((OptimizedData) =>
                                      OptimizedData.computeResourceInputVO.workload
                                        .find(
                                          (wl) => wl.key === 'cloudProvider'
                                        )
                                        .value.includes(cp.name)
                                    )
                                    .map((VM, index) => {
                                      return VM.serverPricing.map(
                                        (sp, spIndex) => {
                                          return (
                                            <tr key={spIndex}>
                                              <td>
                                                {
                                                  VM.serverAttributes.instance
                                                    .instanceName
                                                }
                                              </td>
                                              <td>
                                                {VM.serverAttributes.virtualCpu}
                                              </td>
                                              <td>
                                                {VM.serverAttributes.memory}
                                              </td>
                                              <td>
                                                {sp.price} / {sp.hourlyPrice}
                                              </td>
                                            </tr>
                                          );
                                        }
                                      );
                                    })}
                              </tbody>
                            </Table>
                          </Card.Body>
                        </Accordion.Collapse>
                      </Card>
                    </Accordion>
                  </Col>
                </>
              );
            })}
        </Row>
      </Container>
    </>
  );
};

CompareOptimize.propTypes = {
  cloudProviders: PropTypes.array.isRequired,
  definedData: PropTypes.object.isRequired,
};

export default CompareOptimize;
