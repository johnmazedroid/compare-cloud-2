import React, { useState } from 'react';
import { useFieldArray } from 'react-hook-form';
import { Row, Col, Form, Button, Controller } from 'react-bootstrap';
import { PlusCircle, XCircle } from 'react-bootstrap-icons';

// Components
import Message from '@maze/shared-components/Message/Message.component';

// For Translation
import i18next from 'i18next';

const NestedArray = ({
  nestIndex,
  control,
  register,
  errors,
  stateMsgCreateNewWorkload,
  instanceTypeOptions,
  operatingSystemOptions,
  instanceFamilyOptions,
  regionOptions,
}) => {
  const { fields, remove, append } = useFieldArray({
    control,
    name: `workload[${nestIndex}].computeResource`,
  });

  const [payAsYouGoState, setPayAsYouGo] = useState({});
  const { workload } = errors;

  const onPayAsYouGoChange = ({ target: { id, checked } }) => {
    setPayAsYouGo({
      ...payAsYouGoState,
      [id]: checked,
    });
  };

  return (
    <>
      <h5 className={'px-3'}>{i18next.t('headerComputeResources')}</h5>
      <div className={'m-0 px-3 pb-3 compute-resource-dynamic-row-outer'}>
        {!fields.length && stateMsgCreateNewWorkload && (
          <Message
            data={{
              message: stateMsgCreateNewWorkload,
            }}
          />
        )}
        {!fields.length && (
          <div className={'text-black-50 border-bottom py-3'}>
            {i18next.t('common:noDataFound')}
          </div>
        )}
        {fields.map((item, k) => {
          return (
            <div
              key={item.id}
              className={'compute-resource-dynamic-row border-bottom mt-3'}
            >
              <Row>
                <Col xs={10} className={'pr-0'}>
                  <Form.Group
                    controlId="formWorkloadName"
                    className={'required'}
                  >
                    <Form.Control
                      size={'sm'}
                      autoComplete="off"
                      type="text"
                      placeholder={i18next.t('workloadName')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].workloadId`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('workloadName'),
                            }),
                          },
                        }
                      )}
                      defaultValue={item.workloadId}
                    />
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={
                            workload[nestIndex]?.computeResource[k]?.workloadId
                          }
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col xs={2}>
                  <PlusCircle
                    className={'cursor-pointer'}
                    size={32}
                    onClick={() =>
                      append({
                        workloadId: '',
                        instanceType: '',
                        operatingSystemFamily: '',
                      })
                    }
                  />

                  <XCircle
                    className={'cursor-pointer ml-3'}
                    size={32}
                    onClick={() => remove(k)}
                  />
                </Col>
              </Row>
              <Row>
                <Col md={3}>
                  <Form.Group controlId="formInstanceType">
                    <Form.Label className={'required text-black-50'}>
                      {i18next.t('instanceType')}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      as={'select'}
                      placeholder={i18next.t('instanceType')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].instanceType`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('instanceType'),
                            }),
                          },
                        }
                      )}
                      defaultValue={item.instanceType}
                      custom
                    >
                      <option value={''}>
                        {i18next.t('common:pleaseSelect')}
                      </option>
                      {instanceTypeOptions.map(
                        (instanceType, instanceTypeIndex) => {
                          return (
                            <option key={instanceTypeIndex}>
                              {instanceType.label}
                            </option>
                          );
                        }
                      )}
                    </Form.Control>
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={
                            workload[nestIndex]?.computeResource[k]
                              ?.instanceType
                          }
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group controlId="formOperatingSystem">
                    <Form.Label className={'required text-black-50'}>
                      {i18next.t('operatingSystem')}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      as={'select'}
                      placeholder={i18next.t('operatingSystem')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].operatingSystemFamily`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('operatingSystem'),
                            }),
                          },
                        }
                      )}
                      defaultValue={item.operatingSystemFamily}
                      custom
                    >
                      <option value={''}>
                        {i18next.t('common:pleaseSelect')}
                      </option>
                      {operatingSystemOptions.map(
                        (operatingSystem, operatingSystemIndex) => {
                          return (
                            <option key={operatingSystemIndex}>
                              {operatingSystem.label}
                            </option>
                          );
                        }
                      )}
                    </Form.Control>
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={
                            workload[nestIndex]?.computeResource[k]
                              ?.operatingSystemFamily
                          }
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group controlId="formInstanceFamily">
                    <Form.Label className={'required text-black-50'}>
                      {i18next.t('instanceFamily')}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      as={'select'}
                      placeholder={i18next.t('instanceFamily')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].instanceFamily`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('instanceFamily'),
                            }),
                          },
                        }
                      )}
                      defaultValue={item.instanceFamily}
                      custom
                    >
                      <option value={''}>
                        {i18next.t('common:pleaseSelect')}
                      </option>
                      {instanceFamilyOptions.map(
                        (instanceFamily, instanceFamilyIndex) => {
                          return (
                            <option key={instanceFamilyIndex}>
                              {instanceFamily.label}
                            </option>
                          );
                        }
                      )}
                    </Form.Control>
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={
                            workload[nestIndex]?.computeResource[k]
                              ?.instanceFamily
                          }
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group controlId="formRegion">
                    <Form.Label className={'required text-black-50'}>
                      {i18next.t('regionGroup')}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      as={'select'}
                      placeholder={i18next.t('regionGroup')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].region`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('regionGroup'),
                            }),
                          },
                        }
                      )}
                      defaultValue={item.region}
                      custom
                    >
                      <option value={''}>
                        {i18next.t('common:pleaseSelect')}
                      </option>
                      {regionOptions.map((region, regionIndex) => {
                        return (
                          <option key={regionIndex}>{region.label}</option>
                        );
                      })}
                    </Form.Control>
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={workload[nestIndex]?.computeResource[k]?.region}
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={2}>
                  <Form.Group controlId="formNoOfServers">
                    <Form.Label className={'required text-black-50'}>
                      {i18next.t('noOfServers')}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      type={'number'}
                      placeholder={i18next.t('noOfServers')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].noOfServers`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('noOfServers'),
                            }),
                          },
                          max: {
                            value: 10,
                            message: i18next.t('common:maxValue', {
                              value: 10,
                            }),
                          },
                          min: {
                            value: 1,
                            message: i18next.t('common:minValue', {
                              value: 1,
                            }),
                          },
                        }
                      )}
                      defaultValue={item.noOfServers ? item.noOfServers : 10}
                      min={0}
                      max={10}
                    />
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={
                            workload[nestIndex]?.computeResource[k]?.noOfServers
                          }
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col md={2}>
                  <Form.Group controlId="formVCPUsPerServer">
                    <Form.Label className={'required text-black-50'}>
                      {i18next.t('vCPUsPerServer')}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      type={'number'}
                      placeholder={i18next.t('vCPUsPerServer')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].virtualCpu`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('vCPUsPerServer'),
                            }),
                          },
                          max: {
                            value: 10,
                            message: i18next.t('common:maxValue', {
                              value: 10,
                            }),
                          },
                          min: {
                            value: 1,
                            message: i18next.t('common:minValue', {
                              value: 1,
                            }),
                          },
                        }
                      )}
                      defaultValue={item.virtualCpu ? item.virtualCpu : 10}
                      min={0}
                      max={10}
                    />
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={
                            workload[nestIndex]?.computeResource[k]?.virtualCpu
                          }
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group controlId="formMemoryPerServer">
                    <Form.Label className={'required text-black-50'}>
                      {i18next.t('memoryPerServer')}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      type={'number'}
                      placeholder={i18next.t('memoryPerServer')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].memory`,
                        {
                          required: {
                            value: true,
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('memoryPerServer'),
                            }),
                          },
                          max: {
                            value: 100000,
                            message: i18next.t('common:maxValue', {
                              value: 100000,
                            }),
                          },
                          min: {
                            value: 1,
                            message: i18next.t('common:minValue', {
                              value: 1,
                            }),
                          },
                        }
                      )}
                      defaultValue={item.memory ? item.memory : 100}
                      min={0}
                    />
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={workload[nestIndex]?.computeResource[k]?.memory}
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col md={2}>
                  <Form.Group controlId="formPayAsYouGo">
                    <Form.Label className={'text-black-50'}>
                      {i18next.t('payAsYouGo')}
                    </Form.Label>
                    <Form.Check
                      id={`payAsYouGo${nestIndex}${k}`}
                      type={'switch'}
                      placeholder={i18next.t('payAsYouGo')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].payAsYouGo`
                      )}
                      defaultValue={
                        item.payAsYouGo
                          ? item.payAsYouGo
                          : payAsYouGoState[`payAsYouGo${nestIndex}${k}`]
                      }
                      onChange={onPayAsYouGoChange}
                    />
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group controlId="formEstimatedHoursPerMonth">
                    <Form.Label className={'text-black-50'}>
                      {i18next.t('estimatedHoursPerMonth')} -{' vv '}
                      {payAsYouGoState[`payAsYouGo${nestIndex}${k}`]}
                    </Form.Label>
                    <Form.Control
                      size={'sm'}
                      type={'number'}
                      placeholder={i18next.t('estimatedHoursPerMonth')}
                      {...register(
                        `workload[${nestIndex}].computeResource[${k}].usagePerMonth`,
                        {
                          required: {
                            value:
                              payAsYouGoState[`payAsYouGo${nestIndex}${k}`],
                            message: i18next.t('common:requiredField', {
                              label: i18next.t('estimatedHoursPerMonth'),
                            }),
                          },
                          max: {
                            value: 730,
                            message: i18next.t('common:maxValue', {
                              value: 730,
                            }),
                          },
                          min: {
                            value: 1,
                            message: i18next.t('common:minValue', {
                              value: 1,
                            }),
                          },
                        }
                      )}
                      defaultValue={
                        item.usagePerMonth ? item.usagePerMonth : 730
                      }
                      min={0}
                      max={730}
                      disabled={payAsYouGoState[`payAsYouGo${nestIndex}${k}`]}
                    />
                    <Form.Text>
                      {workload && workload[nestIndex]?.computeResource && (
                        <Message
                          data={
                            workload[nestIndex]?.computeResource[k]
                              ?.usagePerMonth
                          }
                        />
                      )}
                    </Form.Text>
                  </Form.Group>
                </Col>
              </Row>
            </div>
          );
        })}

        <Button
          size={'sm'}
          className={'p-0 text-decoration-none mt-3'}
          variant="link"
          onClick={() =>
            append({
              workloadId: '',
              instanceType: '',
              operatingSystemFamily: '',
            })
          }
        >
          <PlusCircle size={24} />{' '}
          <span className={'ml-1'}>{i18next.t('addServerWorkload')}</span>
        </Button>
      </div>
    </>
  );
};

export default NestedArray;
