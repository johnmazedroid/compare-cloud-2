import React, { useState, useEffect } from 'react';
import { useFieldArray } from 'react-hook-form';
import { Row, Col, Form } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { arrayIntersect } from '@maze/utilities/Array.utilities';
import { errorMessage } from '@maze/utilities/Error.utilities';

// Child Component
import NestedArray from './NestedFieldArray';
import Message from '@maze/shared-components/Message/Message.component';

// Services
import { getDataByAttributeNameHTTPService } from '@maze/http-get-services/attribute-data-http.services';
import { getCloudProvidersHTTPService } from '@maze/http-get-services/cloud-providers-http.services';

// For Translation
import i18next from 'i18next';

const Fields = ({
  control,
  register,
  errors,
  getCloudProviders,
  stateMsgCreateNewWorkload,
}) => {
  const { fields } = useFieldArray({
    control,
    name: 'workload',
  });

  const [selectedCloudProvidersState, setSelectedCloudProviderState] = useState(
    []
  );
  const [cloudProvidersItemsState, setCloudProvidersItemsState] = useState([]);
  const [instanceTypeItemsState, setInstanceTypeItemsState] = useState([]);
  const [operatingSystemItemsState, setOperatingSystemItemsState] = useState(
    []
  );
  const [instanceFamilyItemsState, setInstanceFamilyItemsState] = useState([]);
  const [regionItemsState, setRegionItemsState] = useState([]);

  const [instanceTypeOptionsState, setInstanceTypeOptionsState] = useState([]);
  const [operatingSystemOptionsState, setOperatingSystemOptionsState] =
    useState([]);
  const [instanceFamilyOptionsState, setInstanceFamilyOptionsState] = useState(
    []
  );
  const [regionOptionsState, setRegionOptionsState] = useState([]);

  useEffect(() => {
    let unmounted = false;

    const cloudProviders$ = getCloudProvidersHTTPService();
    const instanceTypeItems$ = getDataByAttributeNameHTTPService({
      attribute: 'instanceType',
      attributeField: 'instanceTypeMapped',
    });
    const operatingSystemItems$ = getDataByAttributeNameHTTPService({
      attribute: 'operatingSystem',
      attributeField: 'operatingSystemFamily',
    });
    const instanceFamilyItems$ = getDataByAttributeNameHTTPService({
      attribute: 'instanceFamily',
      attributeField: 'instanceFamilyMapped',
    });
    const regionItems$ = getDataByAttributeNameHTTPService({
      attribute: 'region',
      attributeField: 'regionGroupMapped',
    });

    cloudProviders$
      .then((response) => {
        if (!unmounted) {
          if (response.length) {
            const cloudProviders = response.map(
              ({ name, cloudProviderId }) => ({
                label: name,
                value: cloudProviderId,
                checked: false,
              })
            );
            setCloudProvidersItemsState(cloudProviders);
            getCloudProviders(response);
          }
        }
      })
      .catch((error) => {
        toast.error(`${errorMessage(error)}`);
      });

    instanceTypeItems$
      .then((response) => {
        if (!unmounted) {
          if (response.length) {
            setInstanceTypeItemsState(response);
          }
        }
      })
      .catch((error) => {
        toast.error(`${errorMessage(error)}`);
      });

    operatingSystemItems$
      .then((response) => {
        if (!unmounted) {
          if (response.length) {
            setOperatingSystemItemsState(response);
          }
        }
      })
      .catch((error) => {
        toast.error(`${errorMessage(error)}`);
      });

    instanceFamilyItems$
      .then((response) => {
        if (!unmounted) {
          if (response.length) {
            setInstanceFamilyItemsState(response);
          }
        }
      })
      .catch((error) => {
        toast.error(`${errorMessage(error)}`);
      });

    regionItems$
      .then((response) => {
        if (!unmounted) {
          if (response.length) {
            setRegionItemsState(response);
          }
        }
      })
      .catch((error) => {
        toast.error(`${errorMessage(error)}`);
      });

    return () => {
      unmounted = true;
    };
  }, []);

  const onChangeCloudProvider = (event) => {
    const selectedCloudProvider = updateSelectedCloudProvider(event);
    setSelectedCloudProviderState(selectedCloudProvider);
    const updateCloudProviderSelection = cloudProvidersItemsState.map((cp) => {
      if (cp.value === event.target.value) {
        return {
          ...cp,
          checked: event.target.checked,
        };
      }

      return { ...cp };
    });

    setCloudProvidersItemsState(updateCloudProviderSelection);
    updateDropDownValues(selectedCloudProvider);
  };

  const updateSelectedCloudProvider = (event) => {
    let selectedCloudProvider = selectedCloudProvidersState;
    const isAvailable = selectedCloudProvider.includes(event.target.value);
    if (event.target.checked) {
      if (!isAvailable) {
        selectedCloudProvider.push(event.target.value);
      }
    } else {
      selectedCloudProvider = selectedCloudProvider.filter(
        (cp) => cp !== event.target.value
      );
    }

    return selectedCloudProvider;
  };

  const updateDropDownValues = (selectedCloudProvider) => {
    const instanceTypes = instanceTypeItemsState;
    const operatingSystems = operatingSystemItemsState;
    const instanceFamily = instanceFamilyItemsState;
    const region = regionItemsState;

    // Filter options based on the Selected Cloud Providers
    const instanceTypeOptions = arrayIntersect(
      instanceTypes,
      selectedCloudProvider
    );
    setInstanceTypeOptionsState(instanceTypeOptions);

    const operatingSystemOptions = arrayIntersect(
      operatingSystems,
      selectedCloudProvider
    );
    setOperatingSystemOptionsState(operatingSystemOptions);

    const instanceFamilyOptions = arrayIntersect(
      instanceFamily,
      selectedCloudProvider
    );
    setInstanceFamilyOptionsState(instanceFamilyOptions);

    const regionOptions = arrayIntersect(region, selectedCloudProvider);
    setRegionOptionsState(regionOptions);
  };

  const { workload } = errors;

  return (
    <>
      {fields.map((item, index) => {
        return (
          <div key={item.id}>
            <Row className={'m-0 pt-3'}>
              <Col md={6}>
                <Form.Group>
                  <Form.Label className={'mr-2 required text-black-50'}>
                    {i18next.t('labelCloudServiceProviders')}
                  </Form.Label>
                  {cloudProvidersItemsState.map((cp, cpIndex) => {
                    return (
                      <Form.Check
                        type={'checkbox'}
                        key={cpIndex}
                        inline
                        label={cp.label}
                        checked={cp.checked}
                        defaultValue={cp.value}
                        {...register(`workload.${index}.cloudProviders`, {
                          required: {
                            value: true,
                            message: i18next.t(
                              'messageSelectAtLeastOneProvide'
                            ),
                          },
                          validate: () => selectedCloudProvidersState.length,
                        })}
                        onChange={onChangeCloudProvider}
                      />
                    );
                  })}
                  <Form.Text>
                    {!cloudProvidersItemsState.some((cp) => cp.checked) && (
                      <Message
                        data={{
                          message: i18next.t('messageSelectAtLeastOneProvide'),
                        }}
                      />
                    )}
                  </Form.Text>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className={'text-md-right'}>
                  <Form.Label
                    htmlFor="tcoTerm"
                    className={'required text-black-50'}
                  >
                    {i18next.t('labelTCOTermYear')}
                  </Form.Label>
                  <Form.Control
                    size={'sm'}
                    {...register(`workload.${index}.tcoTerm`, {
                      required: {
                        value: true,
                        message: i18next.t('common:requiredField', {
                          label: i18next.t('labelTCOTermYear'),
                        }),
                      },
                      max: {
                        value: 5,
                        message: i18next.t('common:maxValue', {
                          value: 5,
                        }),
                      },
                      min: {
                        value: 1,
                        message: i18next.t('common:minValue', {
                          value: 1,
                        }),
                      },
                    })}
                    size={'sm'}
                    className={'ml-3 d-inline w-auto'}
                    type="number"
                    id="tcoTerm"
                    min={1}
                    max={5}
                    defaultValue={item.tcoTerm}
                  />
                  <Form.Text>
                    {workload && <Message data={workload[index].tcoTerm} />}
                  </Form.Text>
                </Form.Group>
              </Col>
            </Row>

            <NestedArray
              nestIndex={index}
              {...{ control, register, errors, stateMsgCreateNewWorkload }}
              instanceTypeOptions={instanceTypeOptionsState}
              operatingSystemOptions={operatingSystemOptionsState}
              instanceFamilyOptions={instanceFamilyOptionsState}
              regionOptions={regionOptionsState}
            />
          </div>
        );
      })}
    </>
  );
};

export default Fields;
