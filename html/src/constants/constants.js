export const Condition = {
  CONTAINS: 'CONTAINS',
  EQUALITY: 'EQUALITY',
  GREATER_THAN: 'GREATER_THAN',
};

export const CustomAttributes = {
  ON_DEMAND: 'On Demand',
  RESERVED: 'Reserved',
};

export const Constants = {
  SELECTED: 'Selected',
  OPTIONAL: 'Optional',
};
