// Libraries
import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import { BrowserRouter } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';

// Components, pages
import App from './App';

// Css, Scss
import './index.scss';

// translation configuration
import './i18n';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('appCloudCompare')
);

module.hot.accept();
reportWebVitals();
