require('dotenv').config();
const express = require('express');
const path = require('path');
const cors = require('cors');

const DataByAttributeNameMockRouter = require('./mock-router/attribute-data');
const CloudProvidersMockRouter = require('./mock-router/cloud-providers');
const ComputeResourcePostMockRouter = require('./mock-router/post-compute-resource');

const app = express();
const port = process.env.port || 3333;

app.use(cors());
app.use(express.json());

app.use('', CloudProvidersMockRouter);
app.use('', DataByAttributeNameMockRouter);
app.use('', ComputeResourcePostMockRouter);

app.get('/cloud-api', (req, res) => {
  res.send('<h1>Welcome Compare Cloud Mock Server!!</h1>');
});

const server = app.listen(port, () =>
  console.log(`Compare Cloud Mock Server listening on port ${port}!`)
);
server.on('error', console.error);
