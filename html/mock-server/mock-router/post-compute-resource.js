const express = require('express');


const ComputeResourceResponseMockData = require('./data/mock-compute-resource-response');
const ComputeResourcePostMockRouter = express.Router();

ComputeResourcePostMockRouter.post('/cloud-api/compare/computeResource', (req, res) => {
  console.log('============================= Compute Resource - POST ==================================');
  console.log('request method :::', req.method);
  console.log('request url :::', req.url);
  console.log('request params :::', req.params);
  console.log('request params :::', req.body);
  console.log('============================= Compute Resource - POST ==================================');
  res.send(ComputeResourceResponseMockData);
});

module.exports = ComputeResourcePostMockRouter;
