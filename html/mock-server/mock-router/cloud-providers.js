const express = require('express');
const CloudProvidersMockData = require('./data/mock-cloud-provider');

const CloudProvidersMockRouter = express.Router();

CloudProvidersMockRouter.get(
  '/cloud-api/data/cloud-provider/all',
  (req, res) => {
    console.log(
      '============================= CLOUD PROVIDER =================================='
    );
    console.log('request method :::', req.method);
    console.log('request url :::', req.url);
    console.log('request params :::', req.params);
    console.log('request params :::', req.body);
    console.log(
      '============================= CLOUD PROVIDER =================================='
    );
    res.send(CloudProvidersMockData);
  }
);

module.exports = CloudProvidersMockRouter;
