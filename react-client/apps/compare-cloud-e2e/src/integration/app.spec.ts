import { navigationBar } from '../support/app.po';

describe('compare-cloud', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    // Custom command example, see `../support/commands.ts` file
    cy.login('my-email@something.com', 'myPassword');

    // Function helper example, see `../support/app.po.ts` file
    navigationBar().contains('Product');
    navigationBar().contains('Services');
    navigationBar().contains('Help');
    navigationBar().should('be.visible');
  });
});
