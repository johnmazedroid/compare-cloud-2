export const InstanceFamilyMockData = [
  {
    "name": "Compute Optimized",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "General Purpose",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "Memory Optimized",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "Storage Optimized",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "GPU Instance",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "Machine Learning ASIC Instances",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "FPGA Instance",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "High Performance Compute",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "NA",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "DEFAULT",
    "cloudProviderIdList": [
      "3"
    ]
  }
];
