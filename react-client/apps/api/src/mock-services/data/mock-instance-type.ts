export const InstanceTypeMockData = [
  {
    "name": "Virtual",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "Dedicated",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "DEFAULT",
    "cloudProviderIdList": [
      "3"
    ]
  }
];
