import {
  GET_INSTANCE_TYPE_LIST_REQUEST,
  GET_INSTANCE_TYPE_LIST_RESPONSE,
  GET_INSTANCE_TYPE_LIST_ERRORS
} from './../constants/types';
import {getDataByAttributeNameHTTPService} from '@react-client/shared';

const instanceTypeRequest = () => {
  // console.log('instanceTypeRequest called');
  return {
    type: GET_INSTANCE_TYPE_LIST_REQUEST,
    data: [],
    errors: {},
    loading: true
  }
}

const getInstanceTypeResponse = (data) => {
  // console.log('getInstanceTypeResponse called :::', data);
  return {
    type: GET_INSTANCE_TYPE_LIST_RESPONSE,
    data,
    errors: {},
    loading: false
  }
}

const getInstanceTypeError = (error) => {
  // console.log('getInstanceTypeError called :::', error);
  return {
    type: GET_INSTANCE_TYPE_LIST_ERRORS,
    data: [],
    errors: error,
    loading: false
  }
}

export const getInstanceType = (context) => (dispatch) => {
  // console.log('getInstanceType called');
  dispatch(instanceTypeRequest())
  getDataByAttributeNameHTTPService(context)
    .then((response) => {
      dispatch(getInstanceTypeResponse(response.data));
    })
    .catch((error) => {
      dispatch(getInstanceTypeError(error))
    })
}
