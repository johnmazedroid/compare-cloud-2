import {
    WORKLOAD_OBJECT
  } from './../constants/types';
  
  export const updateWorkload = (dataObject: any) => {
    // console.log('updateWorkload called', dataObject);
    return {
      type: WORKLOAD_OBJECT,
      data: dataObject,
      errors: {},
      loading: false
    }
  }
  