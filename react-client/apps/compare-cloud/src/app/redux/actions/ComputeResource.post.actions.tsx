import {
  POST_COMPUTE_RESOURCE_ERROR,
  POST_COMPUTE_RESOURCE_REQUEST,
  POST_COMPUTE_RESOURCE_RESPONSE
} from "../constants/types";
import {postComputeResourceHTTPService} from '@react-client/shared';

const postComputeResourceRequest = () => {
  return {
    type: POST_COMPUTE_RESOURCE_REQUEST,
    data: [],
    errors: {},
    loading: true
  }
}

const postComputeResourceResponse = (data) => {
  return {
    type: POST_COMPUTE_RESOURCE_RESPONSE,
    data,
    errors: {},
    loading: false
  }
}

const postComputeResourceError = (error) => {
  return {
    type: POST_COMPUTE_RESOURCE_ERROR,
    data: [],
    errors: error,
    loading: false
  }
}

export const postComputeResources = (requestPayload) => (dispatch) => {
  // console.log('getCloudProviders called');
  dispatch(postComputeResourceRequest())
  postComputeResourceHTTPService(requestPayload)
    .then((response) => {
      dispatch(postComputeResourceResponse(response.data));
    })
    .catch((error) => {
      dispatch(postComputeResourceError(error))
    });
}
