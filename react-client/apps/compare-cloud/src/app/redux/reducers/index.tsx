import {combineReducers} from "redux";
import {cloudProviderReducer} from './CloudProviders.reducers';
import {instanceTypeReducer} from './InstanceType.reducers';
import {instanceFamilyReducer} from './InstanceFamily.reducers';
import {operatingSystemReducer} from './OperatingSystem.reducers';
import {regionReducer} from './Region.reducers';
import {computeResourcePostReducer} from './ComputeResource.post.reducers';
import {workLoadReducer} from './Workload.reducers';

const rootReducer = combineReducers({
  cloudProviderReducer,
  instanceTypeReducer,
  instanceFamilyReducer,
  operatingSystemReducer,
  regionReducer,
  computeResourcePostReducer,
  workLoadReducer
})

export default rootReducer;
