import {
    GET_REGION_LIST_REQUEST,
    GET_REGION_LIST_RESPONSE,
    GET_REGION_LIST_ERRORS
} from './../constants/types';

const initialState = {
    data: [],
    errors: {} || '',
    loading: false
};

export const regionReducer = ( state = initialState, action ) => {
    // console.log('regionReducer called ::', state, action);
    switch( action.type ) {
        case GET_REGION_LIST_REQUEST: 
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        case GET_REGION_LIST_RESPONSE:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        case GET_REGION_LIST_ERRORS:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        default:
            return state;
    }
}