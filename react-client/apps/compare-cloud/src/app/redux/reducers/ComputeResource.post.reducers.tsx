import {
  POST_COMPUTE_RESOURCE_RESPONSE,
  POST_COMPUTE_RESOURCE_REQUEST,
  POST_COMPUTE_RESOURCE_ERROR
} from './../constants/types';

const initialState = {
  data: [],
  errors: {} || '',
  loading: true
};

export const computeResourcePostReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_COMPUTE_RESOURCE_REQUEST:
      return {
        ...state,
        data: action.data,
        errors: action.errors,
        loading: action.loading
      };
    case POST_COMPUTE_RESOURCE_RESPONSE:
      return {
        ...state,
        data: action.data,
        errors: action.errors,
        loading: action.loading
      };
    case POST_COMPUTE_RESOURCE_ERROR:
      return {
        ...state,
        data: action.data,
        errors: action.errors,
        loading: action.loading
      };
    default:
      return state;
  }
}
