import React, { Component } from 'react';
import { Container } from 'react-bootstrap';

import './Services.scss';

export interface ServicesProps {}

class Services extends Component<ServicesProps> {
  render() {
    return (
      <Container fluid>
        Welcome to services!
      </Container>
    );
  }
}

export default Services;
