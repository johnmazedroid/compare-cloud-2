import { Button, message, Steps, Layout } from 'antd';
import React, { Component } from 'react';
import { connect } from "react-redux";
import i18next from 'i18next';
import { getDataByCloudProvider, StepsInterface, WorkLoadObjectDTO } from '@react-client/shared';

import './Compare.scss';
import DefineWorkload from '../../components/define-workload/define-workload';

import { getCloudProviders } from './../../redux/actions/CloudProviders.actions';
import { getInstanceType } from './../../redux/actions/InstanceType.actions';
import { getInstanceFamily } from './../../redux/actions/InstanceFamily.actions';
import { getOperatingSystem } from './../../redux/actions/OperatingSystem.actions';
import { getRegion } from './../../redux/actions/Region.actions';
import { updateWorkload } from './../../redux/actions/Workload.actions';

export interface CompareProps {
  getCloudProvidersList: () => any[];
  getInstanceTypeList: (instanceContext: any) => {};
  getRegionList: (regionContext: any) => {},
  getInstanceFamilyList: (instanceFamilyContext: any) => {},
  getOperatingSystemList: (operatingSystemContext: any) => {}
  cloudProviders: any;
  instanceTypes: any;
  regions: any;
  operatingSystems: any;
  instanceFamilies: any;
  updateWorkLoadData: (workLoadData: any) => void;
  workLoadObjectData: WorkLoadObjectDTO
}

export interface CompareState {
  current: number;
  workLoadObject: WorkLoadObjectDTO;
  formInputFieldData: {
    cloudProvidersOptions: any[];
    instanceTypeOptions: any[];
    instanceTypesData: any[];
    operatingSystemOptions: any[];
    operatingSystemsData: any[];
    instanceFamilyOptions: any[];
    instanceFamiliesData: any[];
    regionOptions: any[];
    regionsData: any[];
  }
}

class Compare extends Component<CompareProps, CompareState> {
  state: CompareState = {
    current: 0,
    workLoadObject: {
      cloudProviders: [],
      tcoTerm: 3,
      computeResource: []
    },
    formInputFieldData: {
      cloudProvidersOptions: [],
      instanceTypeOptions: [],
      instanceTypesData: [],
      operatingSystemOptions: [],
      operatingSystemsData: [],
      instanceFamilyOptions: [],
      instanceFamiliesData: [],
      regionOptions: [],
      regionsData: []
    }
  }

  constructor(props) {
        super(props);
  }

  componentDidMount = () => {
    this.props.getCloudProvidersList();
    this.props.getInstanceTypeList({
      attribute: 'instanceType'
    });
    this.props.getRegionList({
      attribute: 'region'
    });
    this.props.getOperatingSystemList({
      attribute: 'operatingSystem'
    });
    this.props.getInstanceFamilyList({
      attribute: 'instanceFamily'
    });
  }

  componentDidUpdate = (prevProps) => { // TODO: Code refactor - optimize the code in a better way
    // console.log(this.props.workLoadObjectData);
    // console.log(prevProps.workLoadObjectData);

    // Set Cloud providers
    if(!prevProps.cloudProviders.loading !== !this.props.cloudProviders.loading) {
      const cloudProviderOptions = this.props.cloudProviders.data.map(cloudProvider => ({label: cloudProvider.name, value: cloudProvider.cloudProviderId}));
      if(cloudProviderOptions.length) {
        const newCloudProviderState = {
          formInputFieldData: {
            ...this.state.formInputFieldData,
            cloudProvidersOptions: cloudProviderOptions
          },
          workLoadObject: {
            ...this.state.workLoadObject,
            cloudProviders: cloudProviderOptions.map(cloudProviderOption => cloudProviderOption.value)
          }
        }
        this.setState({...newCloudProviderState});
      }
    }

    // Set Instance Types
    if(!prevProps.instanceTypes.loading !== !this.props.instanceTypes.loading) {
      const instanceTypesData = this.props.instanceTypes.data;
      if(instanceTypesData.length) {
        const instanceTypeOptions = this.getOptionsData(instanceTypesData)
        const newInstanceTypeState = {
          formInputFieldData: {
            ...this.state.formInputFieldData,
            instanceTypesData,
            instanceTypeOptions
          }
        }
        this.setState({...newInstanceTypeState});
      }
    }

    // Set Operating Systems
    if(!prevProps.operatingSystems.loading !== !this.props.operatingSystems.loading) {
      
      const operatingSystemsData = this.props.operatingSystems.data;
      if(operatingSystemsData.length) {
        const operatingSystemOptions = this.getOptionsData(operatingSystemsData)
        const newOperatingSystemsState = {
          formInputFieldData: {
            ...this.state.formInputFieldData,
            operatingSystemsData,
            operatingSystemOptions
          }
        }
        this.setState({...newOperatingSystemsState});
      }      
    }

    // Set Instance Family
    if(!prevProps.instanceFamilies.loading !== !this.props.instanceFamilies.loading) {
      const instanceFamiliesData = this.props.instanceFamilies.data;
      if(instanceFamiliesData.length) {
        const instanceFamilyOptions = this.getOptionsData(instanceFamiliesData)
        const newInstanceFamiliesDataState = {
          formInputFieldData: {
            ...this.state.formInputFieldData,
            instanceFamiliesData,
            instanceFamilyOptions
          }
        }
        this.setState({...newInstanceFamiliesDataState});
      }      
    }

    // Set Regions
    if(!prevProps.regions.loading !== !this.props.regions.loading) {
      const regionsData = this.props.regions.data;
      if(regionsData.length) {
        const regionOptions = this.getOptionsData(regionsData)
        const newRegionDataState = {
          formInputFieldData: {
            ...this.state.formInputFieldData,
            regionsData,
            regionOptions
          }
        }
        this.setState({...newRegionDataState});
      }      
    }
  }

  private getOptionsData = (inputData) => {
    return getDataByCloudProvider(inputData, this.state.workLoadObject.cloudProviders)
  }

  prev = () => {   
    console.log('formInputFieldData :::', this.state.formInputFieldData);
    this.setState({
      current: (this.state.current - 1),
      workLoadObject: {
        ...this.props.workLoadObjectData
      },
      formInputFieldData: {
        ...this.state.formInputFieldData
      }
    });
  }

  next = () => {
    console.log(this.state.workLoadObject);
    this.props.updateWorkLoadData(this.state.workLoadObject);
    this.setState({
      current: (this.state.current + 1),
    });
  }

  private computeResourceStepsContent = () => {
    return (
        <DefineWorkload 
          data={this.state.workLoadObject}
          formInputData={{...this.state.formInputFieldData}}  /> 
    )
  }

  private steps: StepsInterface[] = [
    {
      title: i18next.t('titleDefineWorkLoads'),
      content: () => {
        return this.computeResourceStepsContent()
      }
    },
    {
      title: i18next.t('titleModifyWorkLoad'),
      content: () => (<div>Second-content</div>),
    },
    {
      title: i18next.t('titleCompareAndOptimize'),
      content: () => (<div>'Last-content'</div>),
    },
  ];

  render = () => {
    const { Step } = Steps;
    const { current } = this.state;
    const { Content } = Layout;
    return (
      <>
        <Content className="compare-outer-container">
          <Steps current={current}>
              {this.steps.map(item => (
                <Step key={item.title} title={item.title} />
              ))}
          </Steps>
          <div className="steps-content">{this.steps[current].content()}</div>
          <div className="steps-action p-3">
            {current < this.steps.length - 1 && (
              <Button type="primary" onClick={() => this.next()}>
                {i18next.t('common:next')}
              </Button>
            )}
            {current === this.steps.length - 1 && (
              <Button type="primary" onClick={() => message.success(i18next.t('common:completed'))}>
                {i18next.t('common:done')}
              </Button>
            )}
            {current > 0 && (
              <Button style={{ margin: '0 8px' }} onClick={() => this.prev()}>
                {i18next.t('common:previous')}
              </Button>
            )}
          </div>
        </Content>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    cloudProviders: state.cloudProviderReducer,
    instanceTypes: state.instanceTypeReducer,
    operatingSystems: state.operatingSystemReducer,
    instanceFamilies: state.instanceFamilyReducer,
    regions: state.regionReducer,
    workLoadObjectData: state.workLoadReducer.data
  }
};

const mapDispatchToProps = (dispatch) => ({
  getCloudProvidersList: () => dispatch(getCloudProviders()),
  getInstanceTypeList: (context) => dispatch(getInstanceType(context)),
  getOperatingSystemList: (operatingSystemContext) => dispatch(getOperatingSystem(operatingSystemContext)),
  getInstanceFamilyList: (instanceFamilyContext) => dispatch(getInstanceFamily(instanceFamilyContext)),
  getRegionList: (regionContext) => dispatch(getRegion(regionContext)),
  updateWorkLoadData: (data) => dispatch(updateWorkload(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Compare);

