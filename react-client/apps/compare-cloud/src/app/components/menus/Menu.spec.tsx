import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';

import Menus from './Menus';

describe('Menus', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Menus />, { wrapper: MemoryRouter });
    expect(baseElement).toBeTruthy();
  });
});
