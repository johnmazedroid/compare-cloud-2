import { ComputeResourceType, InputSwitch, insertObjectToArray, LayoutWrapper, NumberSpinner, removeObjectFromArray, SelectField, TextInput, WorkLoadObjectDTO } from '@react-client/shared';
import Form, { Button, Col, Input, Row } from 'antd';
import i18next from 'i18next';
import React, { Component } from 'react';
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import {default as UUID} from "uuid";

import './compute-resource.scss';
import { timeStamp } from 'console';

export interface ComputeResourceProps {
    formFieldData: any;
    formData: WorkLoadObjectDTO;
    getComputeResourceData: (updatedData) => void
}

export interface ComputeResourceState {
    computeResource: ComputeResourceType[];
    formFieldDefaultData: any;
}

class ComputeResource extends Component<ComputeResourceProps, ComputeResourceState> {
    state = {
        computeResource: [],
        formFieldDefaultData: null
    }

    private computeDefaultValues: ComputeResourceType = {
        uniqueIdentifier: null,
        workloadName: null,
        estimatedHoursPerMonth: 780,
        instanceType: null,
        operatingSystem: null,
        instanceFamily: null,
        region: null,
        noOfServers: 10,
        vCPUsPerServer: 10,
        memoryPerServer: 10,
        payAsYouGo: true
    }

    componentDidMount = (): void => {
        console.log('this.props ::::', this.props);

        if(this.props.formData.computeResource && this.props.formData.computeResource.length) {
            this.setState({
                computeResource: this.props.formData.computeResource
            })
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if((prevProps.formFieldData?.cloudProvidersOptions?.length !== this.props.formFieldData?.cloudProvidersOptions.length) ||
            (prevProps.formFieldData?.instanceTypesData?.length !== this.props.formFieldData?.instanceTypesData.length) ||
            (prevProps.formFieldData?.operatingSystemsData?.length !== this.props.formFieldData?.operatingSystemsData.length) ||
            (prevProps.formFieldData?.instanceFamiliesData?.length !== this.props.formFieldData?.instanceFamiliesData.length) ||
            (prevProps.formFieldData?.regionsData?.length !== this.props.formFieldData?.regionsData.length) 
        ) {
            this.setState({
                formFieldDefaultData: {...this.props.formFieldData}
            })
        }

        if((prevProps.formFieldData?.instanceTypeOptions?.length !== this.props.formFieldData?.instanceTypeOptions.length) || 
            (prevProps.formFieldData?.operatingSystemOptions?.length !== this.props.formFieldData?.operatingSystemOptions.length) ||
            (prevProps.formFieldData?.instanceFamilyOptions?.length !== this.props.formFieldData?.instanceFamilyOptions.length) || 
            (prevProps.formFieldData?.regionOptions?.length !== this.props.formFieldData?.regionOptions.length)) {
            this.setState({
                formFieldDefaultData: {...this.props.formFieldData}
            })
        }
    }

    private removeRow = (rowData: ComputeResourceType): void => {
        // To remove row items
        const updatedRow = removeObjectFromArray(this.state.computeResource, 'uniqueIdentifier', rowData.uniqueIdentifier);
        if(updatedRow) {
            this.setState({
                computeResource: [...this.state.computeResource]
            });
        }
    }

    private copyRow = (rowData: ComputeResourceType, index: number): void => {
        const indexToReplace: number = (index + 1);
        const itemToInsert = {
            ...rowData,
            workloadName: i18next.t('common:copyOf') + rowData.workloadName,
            uniqueIdentifier:  UUID.v4()
        }
        insertObjectToArray(this.state.computeResource, indexToReplace, itemToInsert);
        this.setState({
            computeResource: [...this.state.computeResource]
        });
        this.props.getComputeResourceData(this.state.computeResource);
    }

    private createRow = (): void => {
        const computeStateCurrentItems = this.state.computeResource;
        computeStateCurrentItems.push({
            ...this.computeDefaultValues,
            uniqueIdentifier:  UUID.v4()
        })
        this.setState({
            computeResource: computeStateCurrentItems,
        })
        this.props.getComputeResourceData(this.state.computeResource);
    }

    private onChangeValue = (value:(string | number | boolean), computeData: ComputeResourceType, fieldName: string, index: number) => {
        const computeResource  = this.state.computeResource;
        const currentItem = computeResource[index];
        currentItem[fieldName] = value;
        computeResource[index] = currentItem;
        this.setState({
            computeResource: [...computeResource]
        });
    }

    render = () => {
        const {computeResource, formFieldDefaultData} = this.state;
        return (
            <LayoutWrapper 
                bgTransparent 
                customClassNames={'bg-transparent dynamic-form-outer-box'}>
                <div className={'dynamic-form-inner-box'}>
                    {
                        computeResource.map((computeData, index) => {
                            return this.computeResourceDynamicForm(computeData, index)
                        })
                    }

                    <Button 
                        className={'p-0 my-3 mx-4'}
                        onClick={this.createRow} 
                        icon={<PlusCircleOutlined style={{ fontSize: '16px' }} />} 
                        title={i18next.t('addComputeResource')} type="link">
                        {i18next.t('addComputeResource')}
                    </Button>
                </div>            
            </LayoutWrapper>
        );
    }

    computeResourceDynamicForm = (computeData: ComputeResourceType, index: number) => {
        const {formFieldDefaultData} = this.state;
        return (
            <LayoutWrapper bgTransparent key={index} customClassNames={'dynamic-form-row'}>
                <>
                    <Row>
                        <Col lg={22} xl={22} md={21} sm={20} xs={20}>
                            <TextInput 
                                placeholder={i18next.t('workloadName')}
                                value={computeData.workloadName}
                                allowClear={true}
                                onChange={(event) => {
                                    const value = event.target.value;
                                    this.onChangeValue(value, computeData, 'workloadName', index);
                                }}
                            /> 
                        </Col>
                        <Col lg={2} xl={2} md={3} sm={4} xs={4} className={'px-2'}>
                            <MinusCircleOutlined 
                                style={{ fontSize: '24px' }}
                                onClick={() => this.removeRow(computeData)} 
                                title={i18next.t('removeComputeResource')} />
                            
                            <PlusCircleOutlined 
                                style={{ fontSize: '24px' }}
                                className={'ml-2'} 
                                onClick={() => this.copyRow(computeData, index)} 
                            />
                        </Col>
                    </Row>
                    <Row className={'mt-2'}>
                        <Col md={6} sm={24} xs={24}>
                            <LayoutWrapper bgTransparent>
                                <SelectField 
                                    showSearch
                                    optionFilterProp="label"
                                    filterOption={ (input, option) =>
                                        option.label.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                    value={computeData.instanceType}
                                    placeholder={i18next.t('instanceType')} 
                                    label={i18next.t('instanceType')} 
                                    options={formFieldDefaultData.instanceTypeOptions}
                                    onChange={(value) => {
                                        this.onChangeValue(value, computeData, 'instanceType', index)
                                    }}
                                /> 
                            </LayoutWrapper>
                        </Col>
                        <Col md={6} sm={24} xs={24} className={'px-md-2 my-xs-2 my-sm-2 my-md-0 my-lg-0 my-xl-0'}>
                            <LayoutWrapper bgTransparent>
                                <SelectField 
                                    showSearch
                                    optionFilterProp="label"
                                    filterOption={ (input, option) =>
                                        option.label.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                    value={computeData.operatingSystem}
                                    placeholder={i18next.t('operatingSystem')} 
                                    label={i18next.t('operatingSystem')}
                                    options={formFieldDefaultData.operatingSystemOptions}
                                    onChange={(value) => {
                                        this.onChangeValue(value, computeData, 'operatingSystem', index)
                                    }}
                                />  
                            </LayoutWrapper>
                        </Col>
                        <Col md={6} sm={24} xs={24}>
                            <LayoutWrapper bgTransparent>
                                <SelectField 
                                    showSearch
                                    optionFilterProp="label"
                                    filterOption={ (input, option) =>
                                        option.label.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                    placeholder={i18next.t('instanceFamily')}
                                    label={i18next.t('instanceFamily')} 
                                    value={computeData.instanceFamily}
                                    options={formFieldDefaultData.instanceFamilyOptions}
                                    onChange={(value) => {
                                        this.onChangeValue(value, computeData, 'instanceFamily', index)
                                    }}
                                />  
                            </LayoutWrapper>
                        </Col>
                        <Col md={6} sm={24} xs={24} className={'pl-md-2 mt-xs-2 mt-sm-2 mt-md-0 mt-lg-0 mt-xl-0'}>
                            <LayoutWrapper bgTransparent>
                                <SelectField 
                                    showSearch
                                    optionFilterProp="label"
                                    filterOption={ (input, option) =>
                                        option.label.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                    placeholder={i18next.t('region')} 
                                    label={i18next.t('region')}
                                    value={computeData.region}
                                    options={formFieldDefaultData.regionOptions}
                                    onChange={(value) => {
                                        this.onChangeValue(value, computeData, 'region', index)
                                    }}
                                /> 
                            </LayoutWrapper>
                        </Col>
                    </Row>
                    <Row className={'mt-2'}>
                      <Col md={6} sm={24} xs={24}>
                            <LayoutWrapper bgTransparent>
                                <NumberSpinner 
                                    min={1}
                                    defaultValue={computeData.noOfServers}
                                    value={computeData.noOfServers}
                                    placeholder={i18next.t('noOfServers')}
                                    customProperties={{
                                        showLabel: true,
                                        label: i18next.t('noOfServers')
                                    }}
                                    onChange={(value) => {
                                        this.onChangeValue(value, computeData, 'noOfServers', index);
                                    }}
                                />
                            </LayoutWrapper>
                      </Col>
                      <Col md={6} sm={24} xs={24} className={'px-md-2 my-xs-2 my-sm-2 my-md-0 my-lg-0 my-xl-0'}>
                          <LayoutWrapper bgTransparent>
                            <NumberSpinner 
                                min={1}
                                defaultValue={computeData.vCPUsPerServer}
                                value={computeData.vCPUsPerServer}
                                placeholder={i18next.t('vCPUsPerServer')}
                                customProperties={{
                                    showLabel: true,
                                    label: i18next.t('vCPUsPerServer')
                                }}
                                onChange={(value) => {
                                    this.onChangeValue(value, computeData, 'vCPUsPerServer', index);
                                }}
                            />
                          </LayoutWrapper>
                      </Col>
                      <Col md={6} sm={24} xs={24} >
                          <LayoutWrapper bgTransparent>
                            <NumberSpinner 
                                min={1}
                                defaultValue={computeData.memoryPerServer}
                                value={computeData.memoryPerServer}
                                placeholder={i18next.t('memoryPerServer')}
                                customProperties={{
                                    showLabel: true,
                                    label: i18next.t('memoryPerServer')
                                }}
                                onChange={(value) => {
                                    this.onChangeValue(value, computeData, 'memoryPerServer', index);
                                }}
                            />
                          </LayoutWrapper>
                      </Col>
                      <Col md={2} sm={4} xs={4} className={'px-md-2 my-xs-2 my-sm-2 my-md-0 my-lg-0 my-xl-0'}>
                          <InputSwitch 
                            defaultChecked 
                            checked={computeData.payAsYouGo} 
                            title={i18next.t('payAsYouGo')}
                            onChange={(value) => {
                                this.onChangeValue(value, computeData, 'payAsYouGo', index);
                            }} /> 
                      </Col>
                      <Col md={4} sm={20} xs={20} className={'mt-xs-2 mt-sm-2 mt-md-0 mt-lg-0 mt-xl-0'}>
                          <NumberSpinner 
                            disabled={computeData.payAsYouGo}
                            min={1} 
                            max={780} 
                            defaultValue={computeData.estimatedHoursPerMonth}
                            value={computeData.estimatedHoursPerMonth}
                            className={'ml-2'} 
                            customProperties={{
                              showLabel: true,
                              label: i18next.t('estimatedHoursPerMonth')
                            }}
                            onChange={(value) => {
                                this.onChangeValue(value, computeData, 'estimatedHoursPerMonth', index);
                            }}
                          /> 
                      </Col>
                    </Row>
                </>
            </LayoutWrapper>
        )
    }
    
}

export default ComputeResource;