import React from 'react';
import { InputSwitch, LayoutWrapper, NumberSpinner, SelectField, TextInput } from '@react-client/shared';
import { Button, Col, Form, Row } from 'antd';
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import i18next from 'i18next';

import './compute-resource.scss';

export interface ComputeResourceProps {
  onNewRowResource: () => void
}

const ComputeResource = (props: ComputeResourceProps) => {
  console.log(props)
  return (
    <LayoutWrapper 
        bgTransparent 
        customClassNames="compute-resource ant-layout m-3 box-shadow-thin bg-transparent"
        title={i18next.t('headerComputeResources')}>
      <Form.List name="computeResource">
        {
          (fields, { add, remove }, { errors }) => (
            <>
              {
                fields.map((field, index) => (
                  <div key={field.key} className="p-3 m-0 dynamic-row bt-gray">
                    <Row>
                        <Col md={22} sm={21} xs={20}>
                          <Form.Item
                            {...field}
                            name={[field.name, 'workloadName']}
                            fieldKey={[field.fieldKey, 'workloadName']}
                            rules={[{ required: true, message: 'Missing first name' }]}
                            className={'mb-0'}
                          >
                            <TextInput placeholder={i18next.t('workloadName')} /> 
                          </Form.Item>
                        </Col>
                        <Col md={2} sm={3} xs={4} className={'px-2'}>
                          <MinusCircleOutlined 
                            style={{ fontSize: '24px' }}
                            onClick={() => remove(field.name)} 
                            title={i18next.t('removeComputeResource')} />
                          
                          <PlusCircleOutlined 
                            style={{ fontSize: '24px' }}
                            className={'ml-2'} 
                            onClick={() => add()} />
                        </Col>
                    </Row>
                    <Row className={'mt-2'}>
                      <Col md={6} sm={24} xs={24}>
                          <Form.Item
                            {...field}
                            name={[field.name, 'instanceType']}
                            fieldKey={[field.fieldKey, 'instanceType']}
                            rules={[{ required: true, message: 'Missing first name' }]}
                            className={'mb-0'}
                            label={i18next.t('instanceType')}
                          >
                            <SelectField placeholder={i18next.t('instanceType')} /> 
                          </Form.Item>
                      </Col>
                      <Col md={6} sm={24} xs={24} className={'px-md-2 my-xs-2 my-sm-2 my-md-0 my-lg-0 my-xl-0'}>
                          <Form.Item
                            {...field}
                            name={[field.name, 'operatingSystem']}
                            fieldKey={[field.fieldKey, 'operatingSystem']}
                            rules={[{ required: true, message: 'Missing first name' }]}
                            className={'mb-0'}
                            label={i18next.t('operatingSystem')} 
                          >
                            <SelectField placeholder={i18next.t('operatingSystem')} />  
                          </Form.Item>
                      </Col>
                      <Col md={6} sm={24} xs={24}>
                          <Form.Item
                            {...field}
                            name={[field.name, 'instanceFamily']}
                            fieldKey={[field.fieldKey, 'instanceFamily']}
                            rules={[{ required: true, message: 'Missing first name' }]}
                            className={'mb-0'}
                            label={i18next.t('instanceFamily')}
                          >
                            <SelectField placeholder={i18next.t('instanceFamily')} />  
                          </Form.Item>
                      </Col>
                      <Col md={6} sm={24} xs={24} className={'pl-md-2 mt-xs-2 mt-sm-2 mt-md-0 mt-lg-0 mt-xl-0'}>
                          <Form.Item
                            {...field}
                            name={[field.name, 'region']}
                            fieldKey={[field.fieldKey, 'region']}
                            rules={[{ required: true, message: 'Missing first name' }]}
                            className={'mb-0'}
                            label={i18next.t('region')}
                          >
                            <SelectField placeholder={i18next.t('region')} /> 
                          </Form.Item>
                      </Col>
                    </Row>
                    <Row className={'mt-2'}>
                      <Col md={6} sm={24} xs={24}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'noOfServers']}
                          fieldKey={[field.fieldKey, 'noOfServers']}
                          rules={[{ required: true, message: 'Missing first name' }]}
                          className={'mb-0'}
                          label={i18next.t('noOfServers')}
                        >
                          <TextInput placeholder={i18next.t('noOfServers')} /> 
                        </Form.Item>
                      </Col>
                      <Col md={6} sm={24} xs={24} className={'px-md-2 my-xs-2 my-sm-2 my-md-0 my-lg-0 my-xl-0'}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'vCPUsPerServer']}
                          fieldKey={[field.fieldKey, 'vCPUsPerServer']}
                          rules={[{ required: true, message: 'Missing first name' }]}
                          className={'mb-0'}
                          label={i18next.t('vCPUsPerServer')}
                        >
                          <TextInput placeholder={i18next.t('vCPUsPerServer')} /> 
                        </Form.Item>
                      </Col>
                      <Col md={6} sm={24} xs={24} >
                        <Form.Item
                          {...field}
                          name={[field.name, 'memoryPerServer']}
                          fieldKey={[field.fieldKey, 'memoryPerServer']}
                          rules={[{ required: true, message: 'Missing first name' }]}
                          className={'mb-0'}
                          label={i18next.t('memoryPerServer')}
                        >
                          <TextInput placeholder={i18next.t('memoryPerServer')} /> 
                        </Form.Item>
                      </Col>
                      <Col md={2} sm={4} xs={4} className={'px-md-2 my-xs-2 my-sm-2 my-md-0 my-lg-0 my-xl-0'}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'payAsYouGo']}
                          fieldKey={[field.fieldKey, 'payAsYouGo']}
                          className={'mb-0'}
                          label={i18next.t('payAsYouGo')}
                        >
                          <InputSwitch defaultChecked checked={true}/> 
                        </Form.Item>
                      </Col>
                      <Col md={4} sm={20} xs={20} className={'mt-xs-2 mt-sm-2 mt-md-0 mt-lg-0 mt-xl-0'}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'estimatedHoursPerMonth']}
                          fieldKey={[field.fieldKey, 'estimatedHoursPerMonth']}
                          rules={[{ required: true, message: 'Missing first name' }]}
                          className={'mb-0'}
                          label={i18next.t('estimatedHoursPerMonth')}
                        >
                          <NumberSpinner 
                            min={1} 
                            max={780} 
                            size={'small'} 
                            className={'ml-2'} 
                            customProperties={{
                              showLabel: false
                            }}
                          /> 
                        </Form.Item>
                      </Col>
                    </Row>
                  </div>
                ))
              }

              <Form.Item className={'m-0 p-1 bt-gray'}>
                  <Button 
                    onClick={() => {
                      add()
                      props.onNewRowResource
                    }} 
                    icon={<PlusCircleOutlined style={{ fontSize: '16px' }} />} 
                    title={i18next.t('addComputeResource')} type="link">
                    {i18next.t('addComputeResource')}
                  </Button>
              </Form.Item>
            </>
          )
        } 
      </Form.List>
    </LayoutWrapper>
  );
}

export default ComputeResource;
