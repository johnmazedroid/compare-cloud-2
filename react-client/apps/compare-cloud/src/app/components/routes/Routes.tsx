import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Spin } from 'antd';

import './Routes.scss';

const Home = lazy(() => import('./../../pages/home/Home'));
const Product = lazy(() => import('./../../pages/product/Product'));
const Services = lazy(() => import('./../../pages/services/Services'));
const Help = lazy(() => import('./../../pages/help/Help'));
const Compare = lazy(() => import('../../pages/compare/Compare'));

export interface RoutesProps {}

const Routes = (props: RoutesProps) => {
  return (
    <Suspense fallback={<Spin size="large" className="spinCenter" />}>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/product" component={Product}/>
        <Route exact path="/services" component={Services}/>
        <Route exact path="/help" component={Help}/>
        <Route exact path="/compare" component={Compare}/>
      </Switch>
    </Suspense>
  );
};

export default Routes;
