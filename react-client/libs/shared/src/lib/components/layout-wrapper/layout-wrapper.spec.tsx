import React from 'react';
import { render } from '@testing-library/react';

import {LayoutWrapper} from './layout-wrapper';

describe('LayoutWrapper', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LayoutWrapper />);
    expect(baseElement).toBeTruthy();
  });
});
