import { Switch, SwitchProps } from 'antd';
import React from 'react';

import './input-switch.module.scss';

export interface InputSwitchProps extends SwitchProps {
}

export const InputSwitch = (props: InputSwitchProps) => {
  return (
    <>
      {props.title && <label className="m-0">{props.title}</label>}
      <Switch {...props} />
    </>
  );
}
