import { Select, SelectProps } from 'antd';
import React from 'react';

import './select-field.module.scss';

export interface SelectFieldProps {
  value?: string;
  options?: Options[];
  optionFilterProp?: string;
  placeholder?: string;
  showSearch?: boolean;
  filterOption?: (input, option) => boolean;
  filterSort?: (optionA: Options, optionB: Options) => number;
  label?: string,
  onChange?: any
}

export interface Options {
  label: string;
  value: string;
}

export const SelectField = (props: SelectFieldProps) => {
  const { Option } = Select;

  return (
    <>
      {props.label && <label className="m-0">{props.label}</label>}
      <Select
        {...props}
      >
      </Select>
    </>
  );
}
