import React from 'react';
import { render } from '@testing-library/react';

import {NumberSpinner} from './number-spinner';

describe('NumberSpinner', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NumberSpinner />);
    expect(baseElement).toBeTruthy();
  });
});
