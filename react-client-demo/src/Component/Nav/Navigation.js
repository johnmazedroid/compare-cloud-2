import React from 'react';
import { NavLink } from 'react-router-dom';

import './Navigation.css';

const Navigation = props => {
  return (
    <header className="main-header">
      <nav className="justify-content-left">
        <ul>
          <li>
            <NavLink to="/" exact>Compare</NavLink>
          </li>
          <li>
            <NavLink to="/compute ">Compute</NavLink>
          </li>
          <li >
            <NavLink to="/help">Help</NavLink>
          </li>
        </ul>
      </nav>
     
    </header>
  );
};

export default Navigation;
