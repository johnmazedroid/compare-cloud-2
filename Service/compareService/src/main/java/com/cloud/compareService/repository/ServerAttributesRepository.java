package com.cloud.compareService.repository;

import com.cloud.compareService.model.Processor;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.ServerAttributes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ServerAttributesRepository extends JpaRepository<ServerAttributes, Integer>
    , JpaSpecificationExecutor<ServerAttributes>,BaseRepository<ServerAttributes> {
    public ServerAttributes findById(int Id);

    public List<ServerAttributes> findAll();

}
