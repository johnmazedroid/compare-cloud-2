package com.cloud.compareService.model;

import com.cloud.compareService.enums.SearchOperation;
import com.cloud.compareService.util.Constant;
import lombok.*;
@Getter
@Setter
public class SearchCriteria {
    private String entity;
    private String key;
    private SearchOperation operation;
    private String value;
    private String[] valueArr;

    public SearchCriteria(String entity, String key, SearchOperation operation, String value) {
        this.entity = entity;
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public SearchCriteria(String entity, String key, SearchOperation operation, String[] valueArr) {
        this.entity = entity;
        this.key = key;
        this.operation = operation;
        this.valueArr = valueArr;
    }

    public SearchCriteria( String key, SearchOperation operation, String value) {
        this.entity = Constant.N_A;
        this.key = key;
        this.operation = operation;
        this.value = value;
    }


}