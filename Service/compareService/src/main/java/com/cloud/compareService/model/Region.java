package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "region_mst")
public class Region implements Serializable {
    @Id
    private int regionMstId;
    private String region;
    private String regionGroupMapped;
    private String locationMapped;
    @JsonIgnore
    private boolean active;
    @JsonIgnore
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cloud_provider_id", nullable=true)
    private CloudProvider cloudProvider ;

}


