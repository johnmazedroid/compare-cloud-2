package com.cloud.compareService.vo;

import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.model.ServerPricing;
import java.util.List;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ComputeResourceOutputVO {
  private ServerAttributes serverAttributes;
  private List<ServerPricingOutputVO> serverPricingOutputVO;
  private ComputeResourceInputVO computeResourceInputVO;

  private float totalPrice;

  private String choice;

  public ComputeResourceOutputVO(ServerAttributes serverAttributes,List<ServerPricingOutputVO> serverPricingOutputVO,ComputeResourceInputVO computeResourceInputVO) {
    this.serverAttributes = serverAttributes;
    this.serverPricingOutputVO = serverPricingOutputVO;
    this.computeResourceInputVO = computeResourceInputVO;
  }



}

