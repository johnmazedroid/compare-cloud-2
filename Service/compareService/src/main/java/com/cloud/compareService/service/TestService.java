package com.cloud.compareService.service;

import com.cloud.compareService.dao.CloudProvidersDaoImpl;
import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.util.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TestService {

    private List<CloudProvider> cloudProviderList;

    @Autowired
    AuthService authService;

    @Autowired
    CloudProvidersDaoImpl cloudProvidersDao;


    public List<CloudProvider> getCloudProvider() {
        return cloudProvidersDao.findAll();
    }

//    public CloudProvider getUser(Long id) {
//        Optional<CloudProvider> userOptional = cloudProviderList.stream().filter(user -> user.getId()==(id)).findFirst();
//        return userOptional.isPresent() ? userOptional.get() : null;
//    }

    public CloudProvider addCloudProvider(CloudProvider cloudProvider) {
        try {
            cloudProvider.setCreatedDate(new Date());
            cloudProvider.setCreatedBy(Constant.ADMIN);
            cloudProvidersDao.saveCloudProvider(cloudProvider);
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw e;

        }
        return cloudProvider;
    }

//    public CloudProvider updateUser(CloudProvider userDto) {
//        Optional<CloudProvider> userOptional = cloudProviderList.stream()
//                .filter(user -> user.getId()==(userDto.getId()))
//                .map(user -> {
//                    user.setName(userDto.getName());
//                    user.setActive(userDto.isActive());
//                    return user;
//                }).findFirst();
//
//        return userOptional.isPresent() ? userOptional.get() : null;
//    }
//
//    public boolean deleteUser(Long id) {
//
//        final int originalSize = cloudProviderList.size();
//        cloudProviderList = cloudProviderList.stream().filter(user -> !(user.getId()==id)).collect(Collectors.toList());
//
//        return originalSize > cloudProviderList.size();
//    }
//
//    public boolean validateUser(Long id) {
//        Optional<CloudProvider> userOptional = cloudProviderList.stream().filter(user -> user.getId()==(id)).findFirst();
//        return userOptional.isPresent() ? userOptional.get().isActive() : false;
//    }
}
