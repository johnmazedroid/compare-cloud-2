package com.cloud.compareService.repository;

import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.model.ServerPricing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface ServerPricingRepository extends JpaRepository<ServerPricing, Integer>
    , JpaSpecificationExecutor<ServerPricing>,BaseRepository<ServerPricing> {

    public ServerPricing findById(int Id);
}
