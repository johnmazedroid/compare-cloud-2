package com.cloud.compareService.specification;

import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.util.Constant;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.cloud.compareService.model.SearchCriteria;

import static com.cloud.compareService.enums.SearchOperation.*;

public  class BaseSpecification<T> implements Specification<T> {
    protected SearchCriteria criteria;

    public BaseSpecification(SearchCriteria criteria) {
        this.criteria=criteria;
    }
    @Override
    public Predicate toPredicate(
        Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        System.out.println("Inside ServerAttributeSpecification toPredicate");
        switch (criteria.getOperation()) {
            case EQUALITY:
                return builder.equal(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.get(criteria.getKey()):root.join(criteria.getEntity()).get(criteria.getKey())
                    , criteria.getValue());
            case NEGATION:
                return builder.notEqual(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.get(criteria.getKey()):root.join(criteria.getEntity()).get(criteria.getKey())
                    , criteria.getValue());
            case GREATER_THAN:
                return builder.greaterThan(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.<String> get(criteria.getKey()):root.join(criteria.getEntity()).<String> get(criteria.getKey())
                    , criteria.getValue().toString());
            case LESS_THAN:
                return builder.lessThan(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.<String> get(criteria.getKey()):root.join(criteria.getEntity()).<String> get(criteria.getKey())
                    , criteria.getValue().toString());
            case LIKE:
                return builder.like(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.<String> get(criteria.getKey()):root.join(criteria.getEntity()).<String> get(criteria.getKey())
                    , criteria.getValue().toString());
            case STARTS_WITH:
                return builder.like(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.<String> get(criteria.getKey()):root.join(criteria.getEntity()).<String> get(criteria.getKey())
                    , criteria.getValue() + "%");
            case ENDS_WITH:
                return builder.like(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.<String> get(criteria.getKey()):root.join(criteria.getEntity()).<String> get(criteria.getKey())
                    , "%" + criteria.getValue());
            case CONTAINS:
                return builder.like(
                    Constant.N_A.equals(criteria.getEntity())?
                        root.<String> get(criteria.getKey()):root.join(criteria.getEntity()).<String> get(criteria.getKey())
                    , "%" + criteria.getValue() + "%");
            case IN:
                In<String> inClause = builder.in (
                     Constant.N_A.equals(criteria.getEntity())?
                        root.<String> get(criteria.getKey()):root.join(criteria.getEntity()).<String> get(criteria.getKey())
                    );
                    for (String value : criteria.getValueArr()) {
                        inClause.value(value);
                    }
                return inClause;

            default:
                return null;
        }
    }
}
