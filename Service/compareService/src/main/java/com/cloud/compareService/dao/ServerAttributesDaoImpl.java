package com.cloud.compareService.dao;

import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.repository.CloudProviderRepository;
import com.cloud.compareService.repository.ServerAttributesRepository;
import com.cloud.compareService.repository.ServerPricingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServerAttributesDaoImpl extends BaseDao<ServerAttributes> {


    @Autowired
    public ServerAttributesDaoImpl(ServerAttributesRepository serverAttributesRepository){
        super(serverAttributesRepository);
    }

    public List<ServerAttributes> findAll() {
        return repository.findAll();
    }



}
