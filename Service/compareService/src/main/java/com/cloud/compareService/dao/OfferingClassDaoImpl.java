package com.cloud.compareService.dao;

import com.cloud.compareService.model.OfferingClass;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.OfferingClassRepository;
import com.cloud.compareService.repository.OperatingSystemRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OfferingClassDaoImpl extends BaseDao<OfferingClass>{

    @Autowired
    public OfferingClassDaoImpl(OfferingClassRepository offeringClassRepository){
        super(offeringClassRepository);
    }

    public OfferingClass save(OfferingClass offeringClass) {
        return repository.save(offeringClass);
    }

    public List<OfferingClass> findAll() {
        return repository.findAll();
    }

}




