package com.cloud.compareService.repository;

import com.cloud.compareService.model.OfferingClass;
import com.cloud.compareService.model.OperatingSystem;
import com.cloud.compareService.model.PaymentOption;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface OperatingSystemRepository extends JpaRepository<OperatingSystem, Integer>
    , JpaSpecificationExecutor<OperatingSystem>,BaseRepository<OperatingSystem>{

    public OperatingSystem findById(int Id);
    
    public List<OperatingSystem> findAll();

}
