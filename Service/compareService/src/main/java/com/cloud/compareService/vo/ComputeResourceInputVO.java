package com.cloud.compareService.vo;


import com.cloud.compareService.util.Constant;
import com.cloud.compareService.util.Util;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ComputeResourceInputVO implements Serializable {
    public ComputeResourceInputFieldVO[] workload;

    public Object getValuebyKey(String key){
        String[] value = new String[0];
        if(!Util.stringNotEmptyOrNull(key)) {throw new IllegalArgumentException("Key is not valid"); };


        for(ComputeResourceInputFieldVO field: workload){
            if(field.key.equalsIgnoreCase(key)){
                value= field.value;
            }
        }
        return value;
    }

    /*Gets first value of the key as String*/
    public String getStringValuebyKey(String key){
        String value = null;
        if(!Util.stringNotEmptyOrNull(key)) {throw new IllegalArgumentException("Key is not valid"); };
        for(ComputeResourceInputFieldVO field: workload){
            if(field.key.equalsIgnoreCase(key)){
                if(Objects.nonNull(field.value) && field.value.length >0) {
                    value = field.value[0];
                }
            }
        }
        return value;
    }

    /*Gets first value of the key as Integer*/
    public int getIntValuebyKey(String key){
        String value = null;
        int defaultVaule=0;
        if(!Util.stringNotEmptyOrNull(key)) {throw new IllegalArgumentException("Key is not valid"); };
        for(ComputeResourceInputFieldVO field: workload){
            if(field.key.equalsIgnoreCase(key)){
                if(Objects.nonNull(field.value) && field.value.length >0) {
                    value = field.value[0];
                }
            }

        }
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultVaule;
        }
    }
}

