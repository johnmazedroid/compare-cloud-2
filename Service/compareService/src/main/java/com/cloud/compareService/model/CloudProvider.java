package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import java.io.Serializable;

import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;


@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cloud_provider_mst")
public class CloudProvider implements Serializable{
    @Id
    @EqualsAndHashCode.Include()
    private String cloudProviderId;
    private String name;
    private String description;
    private int displayOrder;
    @JsonIgnore
    private boolean active;
    @JsonIgnore
    private Date createdDate;
    @JsonIgnore
    private String createdBy;
    @JsonIgnore
    private Date updatedDate;
    @JsonIgnore
    private String updatedBy;



}
