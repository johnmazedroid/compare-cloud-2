package com.cloud.compareService.service;

import com.cloud.compareService.dao.CloudProvidersDaoImpl;
import com.cloud.compareService.dao.RegionDaoImpl;
import com.cloud.compareService.dao.ServerAttributesDaoImpl;
import com.cloud.compareService.enums.SearchOperation;
import com.cloud.compareService.exception.ResponseException;
import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.SearchCriteria;
import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.model.ServerPricing;
import com.cloud.compareService.repository.ServerAttributesRepository;
import com.cloud.compareService.repository.ServerPricingRepository;
import com.cloud.compareService.specification.BaseSpecification;
import com.cloud.compareService.specification.ServerAttributeSpecification;
import com.cloud.compareService.specification.ServerPricingSpecification;
import com.cloud.compareService.util.Constant;
import com.cloud.compareService.util.InputAttribute;
import com.cloud.compareService.util.OutputUtil;
import com.cloud.compareService.util.SearchMapping;
import com.cloud.compareService.util.Util;
import com.cloud.compareService.vo.ComputeResourceInputFieldVO;
import com.cloud.compareService.vo.ComputeResourceInputVO;
import com.cloud.compareService.vo.ComputeResourceOutputVO;
import com.cloud.compareService.vo.ServerPricingOutputVO;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.lang.reflect.Field;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
@Service
public class CompareService {

  private static final Logger LOG = LoggerFactory.getLogger(CompareService.class);

  private List<CloudProvider> cloudProviderList;
  private static final String RESERVED="Reserved";
  private static final String ON_DEMAND="On Demand";



  @Autowired
  AuthService authService;

  @Autowired
  CloudProvidersDaoImpl cloudProvidersDao;

  @Autowired
  RegionDaoImpl RegionDao;

  @Autowired
  ServerAttributesDaoImpl serverAttributesDaoImpl;

  @Autowired
  ServerAttributesRepository serverAttributesRepository;

  @Autowired
  ServerPricingRepository serverPricingRepository;

  public List<CloudProvider> getCloudProvider() {
    return cloudProvidersDao.findAll();
  }

  public List<Region> getRegion() {
    return RegionDao.findAll();
  }

  public List<ComputeResourceOutputVO> computeResource(String payload,Map<String, Object> requestParams)
      throws ResponseException {
    List<ComputeResourceOutputVO> computeResourceOutputVOList = new ArrayList<>();
    List<ServerAttributes> serverAttributesList = new ArrayList<>();
    List<ServerPricing> serverPricingList = new ArrayList<>();

    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

      ComputeResourceInputVO[] ComputeResourceInputList = mapper.readValue(payload, ComputeResourceInputVO[].class);

      for (ComputeResourceInputVO computeResourceInput : ComputeResourceInputList) {

        System.out.println("computeResourceInput" + computeResourceInput);
         serverAttributesList=findServerAttribute(computeResourceInput);

        for(ServerAttributes serverAttributes: serverAttributesList){
          serverPricingList=findServerPricing(serverAttributes.getServerAttributesId(),computeResourceInput);
          LOG.info("Size is "+serverPricingList.size());

          List<ServerPricingOutputVO> serverPricingOutputVOList = OutputUtil.ConvertServerPricingListToServerPricingVOList(
              (ArrayList<ServerPricing>) serverPricingList, computeResourceInput);

          ComputeResourceOutputVO computeResourceOutputVO = new ComputeResourceOutputVO(serverAttributes, serverPricingOutputVOList,computeResourceInput);
          OutputUtil.populateBestPrice(computeResourceOutputVO);
          computeResourceOutputVOList.add(computeResourceOutputVO);
        }

      }

      OutputUtil.populateChoice(computeResourceOutputVOList);




    } catch (Exception e) {
      LOG.error("Error in Compute" , e);
      throw new ResponseException (e.getMessage() );
    }
    return computeResourceOutputVOList;
  }

  private List<ServerPricing> findServerPricing ( int serverAttributesId ,ComputeResourceInputVO computeResourceInputVO)
      throws IllegalAccessException{
    List<BaseSpecification> baseSpecificationList = new ArrayList<>();


    for(ComputeResourceInputFieldVO field: computeResourceInputVO.getWorkload()){
      if(Util.stringNotEmptyOrNull(field.getKey())  && Constant.ServerPricing.equalsIgnoreCase(SearchMapping.valueOf(field.getKey()).parent)
          && field.getValue()!=null && field.getValue().length>0 && Util.stringNotEmptyOrNull(field.getValue()[0])
          && Util.stringNotEmptyOrNull(field.getSearch())
      ){
        SearchMapping searchMapping = SearchMapping.valueOf(field.getKey());
        BaseSpecification spec;

       if(field.getKey().equals("leaseTerm") && field.getValue()[0].equals(RESERVED) ){
          spec = new ServerAttributeSpecification(
              new SearchCriteria(searchMapping.entity, searchMapping.attribute,SearchOperation.NEGATION,ON_DEMAND));
        }
        else
         spec = new ServerAttributeSpecification(
            new SearchCriteria(searchMapping.entity, searchMapping.attribute,SearchOperation.valueOf(field.getSearch()),field.getValue()[0]));
        baseSpecificationList.add(spec);

      }
    }


    SearchMapping searchMapping = SearchMapping.valueOf("serverAttribute");
    Specification specs = Specification.where(new ServerPricingSpecification(
        new SearchCriteria(searchMapping.entity, searchMapping.attribute,SearchOperation.EQUALITY, Integer.toString(serverAttributesId)))
    );
    for (Specification spec : baseSpecificationList) {
      specs = specs.and(spec);
    }

    List<ServerPricing> serverPricingList = serverPricingRepository.findAll(specs);
    return serverPricingList;
  }




  private List<ServerAttributes> findServerAttribute(ComputeResourceInputVO computeResourceInputVO)
      throws IllegalAccessException {

    List<BaseSpecification> baseSpecificationList = new ArrayList<>();

    for(ComputeResourceInputFieldVO field: computeResourceInputVO.getWorkload()){
      if(Util.stringNotEmptyOrNull(field.getKey())  && Constant.ServerAttributes.equalsIgnoreCase(SearchMapping.valueOf(field.getKey()).parent)
          && field.getValue()!=null && field.getValue().length>0 && Util.stringNotEmptyOrNull(field.getValue()[0])
            && Util.stringNotEmptyOrNull(field.getSearch())
      ){
        SearchMapping searchMapping = SearchMapping.valueOf(field.getKey());

        BaseSpecification spec = new BaseSpecification(
            new SearchCriteria(searchMapping.entity, searchMapping.attribute,SearchOperation.valueOf(field.getSearch()), field.getValue()[0]));
        baseSpecificationList.add(spec);

      }
    }

    Specification specs = Specification.where(null);
    for (Specification spec : baseSpecificationList) {
      specs = specs.and(spec);
    }

    List<ServerAttributes> serverAttributesList = serverAttributesRepository.findAll(specs);
    return serverAttributesList;
  }

}
