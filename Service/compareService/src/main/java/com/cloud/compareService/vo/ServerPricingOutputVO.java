package com.cloud.compareService.vo;

import com.cloud.compareService.model.LeaseTerm;
import com.cloud.compareService.model.OfferingClass;
import com.cloud.compareService.model.PaymentOption;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.Set;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ServerPricingOutputVO {
  public int serverAttributesId;
  public float price;
  public String leaseTermMapped;
  public String offeringClassMapped;
  public String paymentOptionMapped;

}

