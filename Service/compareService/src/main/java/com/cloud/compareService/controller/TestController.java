package com.cloud.compareService.controller;

import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.service.AuthService;
import com.cloud.compareService.service.CompareService;
import com.cloud.compareService.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @Value("${spring.datasource.url}")
    private String databaseUrl;



    @Autowired
    private TestService testService;

    @Autowired
    private AuthService authService;

//    @GetMapping
//    public CloudProvider getCloudProvide(@RequestParam(name = "id") Long id) {
//        return testService.getUser(id);
//    }

//    @RequestMapping(method=RequestMethod.GET, path="/cloud-provider/all")
//    @ResponseBody
//    public List<CloudProvider> getCloudProvider() {
//        System.out.println("databaseUrl " +  databaseUrl);
//        return testService.getCloudProvider();
//    }

//    @GetMapping("/validate")
//    public boolean validateUser(@RequestParam(name = "id") Long id ) {
//        return testService.validateUser(id);
//    }

//    @RequestMapping(method=RequestMethod.POST, path="/cloud-provider/add")
//    @ResponseBody
//    public CloudProvider addCloudProvider(@Validated @RequestBody CloudProvider cloudProviderDto ) {
//        return testService.addCloudProvider(cloudProviderDto);
//    }
//
//    @PutMapping
//    public CloudProvider updateUser(@Validated @RequestBody CloudProvider cloudProviderDto ) {
//        return testService.updateUser(cloudProviderDto);
//    }
//
//    @DeleteMapping
//    public boolean deleteUser(@RequestParam(name = "name") String username, @RequestParam(name = "id") Long id ) {
//        boolean result=authService.isUserValid(username);
//        return testService.deleteUser(id);
//    }
}
