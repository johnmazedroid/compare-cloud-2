package com.cloud.compareService.dao;

import com.cloud.compareService.model.Processor;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.ProcessorRepository;
import com.cloud.compareService.repository.ServerAttributesRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProcessorDaoImpl extends BaseDao<Processor> {

    @Autowired
    public ProcessorDaoImpl(ProcessorRepository processorRepository){
        super(processorRepository);
    }

    public Processor save(Processor processor) {
        return repository.save(processor);
    }

    public List<Processor> findAll() {
        return repository.findAll();
    }

}




