package com.cloud.compareService.controller;

import com.cloud.compareService.exception.ResponseException;
import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.service.AuthService;
import com.cloud.compareService.service.CompareService;
import com.cloud.compareService.service.DataService;
import com.cloud.compareService.service.TestService;
import com.cloud.compareService.vo.ComputeResourceOutputVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/compare")
public class CompareController {

    private static final Logger LOG = LoggerFactory.getLogger(CompareController.class);


    @Value("${spring.datasource.url}")
    private String databaseUrl;


    @Autowired
    private CompareService compareService;

    @Autowired
    private DataService dataService;

    @Autowired
    private AuthService authService;




    @RequestMapping(method = RequestMethod.POST, path = "/computeResource")
    @ResponseBody
    public List<ComputeResourceOutputVO> computeResource(@RequestBody String payload, @RequestParam Map<String, Object> requestParams)
            throws ResponseException {
        List<ComputeResourceOutputVO> computeResourceOutputVOList;
        System.out.println("databaseUrl " + databaseUrl);


        try {
            computeResourceOutputVOList= compareService.computeResource(payload ,requestParams);
        } catch (Exception e) {
            System.out.println("Inside Exception" + e.getMessage());
            throw new ResponseException(e);

        }
    return computeResourceOutputVOList;

    }
}
