package com.cloud.compareService.repository;

import com.cloud.compareService.model.ApplicationProperties;
import com.cloud.compareService.model.Instance;
import com.cloud.compareService.model.InstanceFamily;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface ApplicationPropertiesRepository extends JpaRepository<ApplicationProperties, Integer>
    , JpaSpecificationExecutor<ApplicationProperties>,BaseRepository<ApplicationProperties>{

    public ApplicationProperties findById(int Id);

    public List<ApplicationProperties> findAll();

}
