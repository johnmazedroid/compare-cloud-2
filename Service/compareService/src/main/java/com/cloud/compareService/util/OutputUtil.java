package com.cloud.compareService.util;

import static java.util.stream.Collectors.groupingBy;

import com.cloud.compareService.controller.CompareController;
import com.cloud.compareService.model.ServerPricing;
import com.cloud.compareService.vo.ComputeResourceInputVO;
import com.cloud.compareService.vo.ComputeResourceOutputVO;
import com.cloud.compareService.vo.ServerPricingOutputVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import javassist.bytecode.stackmap.BasicBlock.Catch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OutputUtil {

    private static final String RESERVED="Reserved";
    private static final String ON_DEMAND="On Demand";
    private static final String YEAR="YR";

    private static final Logger LOG = LoggerFactory.getLogger(CompareController.class);


    public static void populateChoice(List<ComputeResourceOutputVO> computeResourceOutputVOList) {
        float lowestPrice=getSALowestPrice(computeResourceOutputVOList);
        for(ComputeResourceOutputVO computeResourceOutputVO: computeResourceOutputVOList){

                if ((computeResourceOutputVO.getTotalPrice() == lowestPrice)) {
                    computeResourceOutputVO.setChoice("Selected");
                } else {
                    computeResourceOutputVO.setChoice("Optional");
                }

        }
    }

  /*  public static float getSPLowestPrice(List<ComputeResourceOutputVO> computeResourceOutputVOList) {
        float lowestPrice=Float.MAX_VALUE;
        for(ComputeResourceOutputVO computeResourceOutputVO: computeResourceOutputVOList){
            for(ServerPricing sp: computeResourceOutputVO.getServerPricing() ){
                if(sp.getPrice() < lowestPrice){
                    lowestPrice = sp.getPrice();
                }
            }
        }
        return lowestPrice;
    }

   */

    public static float getSALowestPrice(List<ComputeResourceOutputVO> computeResourceOutputVOList) {
        float lowestTotalPrice=Float.MAX_VALUE;
        for(ComputeResourceOutputVO computeResourceOutputVO: computeResourceOutputVOList){
                if(computeResourceOutputVO.getTotalPrice() < lowestTotalPrice){
                    lowestTotalPrice = computeResourceOutputVO.getTotalPrice() ;
                }
        }
        return lowestTotalPrice;
    }

    public static void populateBestPrice(ComputeResourceOutputVO computeResourceOutputVO) {
        float totalPrice=computeResourceOutputVO.getTotalPrice();
        for(ServerPricingOutputVO sp: computeResourceOutputVO.getServerPricingOutputVO() ){
            if(totalPrice==0  || totalPrice>sp.getPrice()){
                computeResourceOutputVO.setTotalPrice(sp.getPrice());
            }
        }
    }

    public static List<ServerPricingOutputVO> ConvertServerPricingListToServerPricingVOList
        (ArrayList<ServerPricing> serverPricingList , ComputeResourceInputVO computeResourceInput){
        List<ServerPricingOutputVO> serverPricingOutputVOList = new ArrayList<ServerPricingOutputVO>();
        if(Objects.isNull(serverPricingList) || serverPricingList.size()==0){
            return serverPricingOutputVOList;
        }
        int tcoTerm = computeResourceInput.getIntValuebyKey("tcoTerm");
        int usagePerMonth = computeResourceInput.getIntValuebyKey("usagePerMonth");


        Map<List<String>, List<ServerPricing>> groupByOfferingClassPaymentOptionMap =
            serverPricingList.stream().collect(Collectors.groupingBy(p -> Arrays.asList(p.getOfferingClass().getOfferingClassMapped(), p.getPaymentOption().getPaymentOptionMapped())));

        if(ON_DEMAND.equalsIgnoreCase(computeResourceInput.getStringValuebyKey("leaseTerm"))){
            for (Map.Entry<List<String>, List<ServerPricing>> entry : groupByOfferingClassPaymentOptionMap.entrySet()) {
                ServerPricing sp = entry.getValue().get(0);
                ServerPricingOutputVO spo = new ServerPricingOutputVO(sp.getServerAttributesId()
                    ,0.0f
                    ,computeResourceInput.getStringValuebyKey("leaseTerm")
                    ,sp.getOfferingClass().getOfferingClassMapped()
                    ,sp.getPaymentOption().getPaymentOptionMapped()
                );
                float price = tcoTerm * 12 * usagePerMonth * sp.getHourlyPrice();
                spo.setPrice(price);
                serverPricingOutputVOList.add(spo);

            }
        }

        if(RESERVED.equalsIgnoreCase(computeResourceInput.getStringValuebyKey("leaseTerm"))){

            for (Map.Entry<List<String>, List<ServerPricing>> entry : groupByOfferingClassPaymentOptionMap.entrySet()) {

                Set<Integer> availableYears = getAvailableYearsFromSPList(entry.getValue());
                ArrayList<Integer> minYearSum = minSum(availableYears , tcoTerm);

                ServerPricing sp = entry.getValue().get(0);
                ServerPricingOutputVO spo = new ServerPricingOutputVO(sp.getServerAttributesId()
                    ,0.0f
                    ,computeResourceInput.getStringValuebyKey("leaseTerm")
                    ,sp.getOfferingClass().getOfferingClassMapped()
                    ,sp.getPaymentOption().getPaymentOptionMapped()
                );
                float price =0.0f;
                for(Integer noOfYear: minYearSum){
                    price = price+ ( noOfYear * 12 * usagePerMonth * getPriceByYear(noOfYear, entry.getValue()) );
                }

                spo.setPrice(price);
                serverPricingOutputVOList.add(spo);

            }

        }

        return serverPricingOutputVOList;
    }

    private static float getPriceByYear(int year , List<ServerPricing> spList){
        float result =0;
        for (ServerPricing sp:spList){
            String yearStr = sp.getLeaseTerm().getLeaseTermMapped().substring(0,
                sp.getLeaseTerm().getLeaseTermMapped().length() - YEAR.length());
            int currentYear = Integer.valueOf(yearStr);
            if (year == currentYear){
                return sp.getHourlyPrice();
            }
        }
        return result;
    }

    private static Set<Integer> getAvailableYearsFromSPList(List<ServerPricing> spList){
        Set<Integer> availableYears = new TreeSet<Integer>(Collections.reverseOrder());
        for(ServerPricing sp: spList){
            if(sp.getLeaseTerm().getLeaseTermMapped().toUpperCase().contains(YEAR)){
                Integer year = null;
               try {
                   String yearStr = sp.getLeaseTerm().getLeaseTermMapped().substring(0,
                       sp.getLeaseTerm().getLeaseTermMapped().length() - YEAR.length());
                    year = Integer.valueOf(yearStr);
               }catch(Exception e){
                   LOG.warn("Error in parsing Year {}",sp.getLeaseTerm().getLeaseTermMapped(), e);
               }
                if(year >0)
                availableYears.add(year);
            }
        }
        return availableYears;
    }

    /* assuming value 1 is available in input */
    private static ArrayList<Integer> minSum(Set<Integer> inputArr , int sum) {
        ArrayList<Integer> resultArr = new ArrayList<>();
        int tempSum =0;
        for (Integer value : inputArr){
            if(tempSum + value > sum)
                continue;
            while ( tempSum + value <=sum){
                tempSum = tempSum + value;
                resultArr.add(value);
            }
        }

        return resultArr;
    }
}
