package com.cloud.compareService.util;


public enum InputAttribute {
  region("region", "locationMapped"),
  cloudProvider("cloudProvider", "name"),
  instance("instance", "instanceName"),
  instanceType("instanceType","instanceTypeMapped"),
  instanceFamily("instanceFamily","instanceFamilyMapped"),
  processor("processor","processorFamilyMapped" ),
  operatingSystem("operatingSystem","operatingSystemMapped"),
  operatingSystemFamily("operatingSystem","operatingSystemFamily"),
  leaseTerm("leaseTerm", "leaseTermMapped"),
  offeringClass("offeringClass", "offeringClassMapped"),
  paymentOption("paymentOption", "paymentOptionMapped");


  public String entity;
  public String attribute;
  InputAttribute(String entity,String attribute) {
    this.entity = entity;
    this.attribute=attribute;
  }

}


