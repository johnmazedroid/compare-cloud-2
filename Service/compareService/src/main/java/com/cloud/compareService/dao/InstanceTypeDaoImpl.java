package com.cloud.compareService.dao;

import com.cloud.compareService.model.InstanceType;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.InstanceTypeRepository;
import com.cloud.compareService.repository.LeaseTermRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InstanceTypeDaoImpl extends BaseDao<InstanceType> {


    @Autowired
    public InstanceTypeDaoImpl(InstanceTypeRepository instanceTypeRepository){
        super(instanceTypeRepository);
    }

    public InstanceType save(InstanceType instanceType) {
        return repository.save(instanceType);
    }

    public List<InstanceType> findAll() {
        return repository.findAll();
    }

}




