package com.cloud.compareService.repository;
import com.cloud.compareService.model.ApplicationProperties;
import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.InstanceFamily;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



@Repository
public interface CloudProviderRepository extends JpaRepository<CloudProvider, Integer>
    , JpaSpecificationExecutor<CloudProvider>,BaseRepository<CloudProvider>{

    public CloudProvider findById(int Id);

    @Query(value=" select * from cloud_provider_mst \n" +
            "WHERE UPPER(name) LIKE :name", nativeQuery=true)
    public List<CloudProvider> findByName(@Param("name") String name );

    public List<CloudProvider> findAll();

}
