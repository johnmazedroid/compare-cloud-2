package com.cloud.compareService.util;

public final class Constant {
    public static final String ADMIN ="admin";
    public static final String N_A ="Not Applicable";

    public static final String ServerAttributes="ServerAttributes";
    public static final String ServerPricing="ServerPricing";

    public static final String ERR_DATA_NOT_AVAILABLE = "Data Not Available";
}
