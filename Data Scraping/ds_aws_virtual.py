import time
import requests
import mysql.connector
import ds_database

start_time = time.time()
cloud_provider = '1'
instance_type = 'Virtual'

# Initialize database
db = ds_database.Database(cloud_provider, instance_type)

def build_url(os, region):
	url = base_url.replace('OPERATING_SYSTEM', os)
	url = url.replace('REGION', region)
	return url

# For reference: https://aws.amazon.com/ec2/pricing/reserved-instances/pricing/
base_url = 'https://a0.p.awsstatic.com/pricing/1.0/ec2/region/REGION/reserved-instance/OPERATING_SYSTEM/index.json'
operating_systems = ['linux', 'rhel', 'suse', 'windows', 'windows-std', 'windows-web', 'windows-enterprise', 'linux-std', 'linux-web', 'linux-enterprise']
regions = ['us-east-1', 'us-east-2', 'us-west-1', 'us-west-2', 'af-south-1', 'ap-east-1', 'ap-south-1', 'ap-northeast-1', 'ap-northeast-2', 'ap-northeast-3', 'ap-southeast-1', 'ap-southeast-2', 'ca-central-1', 'eu-central-1', 'eu-west-1', 'eu-west-2', 'eu-south-1', 'eu-west-3', 'eu-north-1', 'me-south-1', 'sa-east-1', 'us-gov-east-1', 'us-gov-west-1', 'us-east-1-wl1', 'us-west-2-lax-1', 'us-west-2-wl1']

# For debugging
# file = open('aws_pricing_virtual.csv', 'w')
operating_systems = ['linux', 'rhel', 'suse', 'windows']
# regions = ['us-east-1', 'us-east-2', 'us-west-1', 'us-west-2']

for x in operating_systems:
	for y in regions:

		server_attributes_list = []
		server_pricing_list = []

		url = build_url(x, y)
		print('Fetching data for URL: ' + url)
    
		try:
			price_json = requests.get(url).json()
		except:
			price_json = {}
			print('Could not fetch data for OS ' + x + ' and Region ' + y)
			continue # move on to next Region in the list

		for i in price_json['prices']:
			instance_name = i['attributes']['aws:ec2:instanceType']
			instance_family = i['attributes']['aws:ec2:instanceFamily']

			virtual_cpu = i['attributes']['aws:ec2:vcpu']
			memory = i['attributes']['aws:ec2:memory']
			default_storage = i['attributes']['aws:ec2:storage']

			lease_term = i['attributes']['aws:offerTermLeaseLength']
			offering_class = i['attributes']['aws:offerTermOfferingClass'].title()
			payment_option = i['attributes']['aws:offerTermPurchaseOption']
			hourly_price = i['calculatedPrice']['effectiveHourlyRate']['USD']
			ondemand_price = i['calculatedPrice']['onDemandRate']['USD']
			
			if 'aws:ec2:physicalProcessor' in i['attributes']:
				processor = i['attributes']['aws:ec2:physicalProcessor']
			else:
				processor = ''

			if 'aws:ec2:clockSpeed' in i['attributes']:
				clock_speed = i['attributes']['aws:ec2:clockSpeed']
				processor = processor + ' ' + clock_speed
			else:
				clock_speed = ''

			if lease_term == '1yr' and offering_class == 'Standard' and payment_option == 'No Upfront':
				attr_tuple = (cloud_provider, x, y, instance_name, virtual_cpu, memory, default_storage, instance_type, instance_family, processor)
				server_attributes_list.append(attr_tuple)

				price_tuple = (cloud_provider, x, y, instance_name, 'On Demand', offering_class, payment_option, ondemand_price)
				server_pricing_list.append(price_tuple)

			price_tuple = (cloud_provider, x, y, instance_name, lease_term, offering_class, payment_option, hourly_price)
			server_pricing_list.append(price_tuple)

		try:
			db.cursor.executemany(db.server_attributes_ins_sql, server_attributes_list)
			db.cursor.executemany(db.server_pricing_ins_sql, server_pricing_list)
			db.connect.commit()
			print('Data inserted successfully.')
		except (mysql.connector.Error, mysql.connector.Warning) as e:
			print(e)

		# For debugging
		# file.write(str(server_attributes_list))

# For debugging
# file.close()

db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
