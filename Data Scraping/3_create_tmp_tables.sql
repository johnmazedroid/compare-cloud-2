DROP TABLE IF EXISTS server_pricing_tmp;
DROP TABLE IF EXISTS server_attributes_tmp;

CREATE TABLE `server_attributes_tmp` (
  `cloud_provider_id` int(2) NOT NULL,
  `operating_system_mst_id` int(11) NOT NULL,
  `region_mst_id` int(11) NOT NULL,
  `instance_mst_id` int(11) NOT NULL,
  `instance_type_mst_id` int(11) NOT NULL,
  `instance_family_mst_id` int(11) NOT NULL,
  `processor_mst_id` int(11) NOT NULL,
  `virtual_cpu` int(11) NOT NULL,
  `memory` float NOT NULL,
  `default_storage` float DEFAULT NULL,
  `server_attributes_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`server_attributes_id`),
  UNIQUE KEY `server_attributes_uk1` (`cloud_provider_id`,`operating_system_mst_id`,`region_mst_id`,`instance_mst_id`),
  KEY `operating_system_mst_id` (`operating_system_mst_id`),
  KEY `region_mst_id` (`region_mst_id`),
  KEY `instance_mst_id` (`instance_mst_id`),
  KEY `instance_type_mst_id` (`instance_type_mst_id`),
  KEY `instance_family_mst_id` (`instance_family_mst_id`),
  KEY `processor_mst_id` (`processor_mst_id`),
  CONSTRAINT `server_attributes_tmp_cp_fk1` FOREIGN KEY (`cloud_provider_id`)       REFERENCES `cloud_provider_mst` (`cloud_provider_id`),
  CONSTRAINT `server_attributes_tmp_os_fk2` FOREIGN KEY (`operating_system_mst_id`) REFERENCES `operating_system_mst` (`operating_system_mst_id`),
  CONSTRAINT `server_attributes_tmp_re_fk3` FOREIGN KEY (`region_mst_id`)           REFERENCES `region_mst` (`region_mst_id`),
  CONSTRAINT `server_attributes_tmp_in_fk4` FOREIGN KEY (`instance_mst_id`)         REFERENCES `instance_mst` (`instance_mst_id`),
  CONSTRAINT `server_attributes_tmp_it_fk5` FOREIGN KEY (`instance_type_mst_id`)    REFERENCES `instance_type_mst` (`instance_type_mst_id`),
  CONSTRAINT `server_attributes_tmp_if_fk6` FOREIGN KEY (`instance_family_mst_id`)  REFERENCES `instance_family_mst` (`instance_family_mst_id`),
  CONSTRAINT `server_attributes_tmp_pr_fk7` FOREIGN KEY (`processor_mst_id`)        REFERENCES `processor_mst` (`processor_mst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `server_pricing_tmp` (
  `server_attributes_id` int(11) NOT NULL,
  `lease_term_mst_id` int(11) NOT NULL,
  `offering_class_mst_id` int(11) NOT NULL,
  `payment_option_mst_id` int(11) NOT NULL,
  `hourly_price` float NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server_pricing_id` int(11) NOT NULL,
  PRIMARY KEY (`server_pricing_id`),
  UNIQUE KEY `server_pricing_uk1` (`server_attributes_id`,`lease_term_mst_id`,`offering_class_mst_id`,`payment_option_mst_id`),
  KEY `lease_term_mst_id` (`lease_term_mst_id`),
  KEY `offering_class_mst_id` (`offering_class_mst_id`),
  KEY `payment_option_mst_id` (`payment_option_mst_id`),
  CONSTRAINT `server_pricing_tmp_lt_fk1` FOREIGN KEY (`lease_term_mst_id`)     REFERENCES `lease_term_mst` (`lease_term_mst_id`),
  CONSTRAINT `server_pricing_tmp_oc_fk2` FOREIGN KEY (`offering_class_mst_id`) REFERENCES `offering_class_mst` (`offering_class_mst_id`),
  CONSTRAINT `server_pricing_tmp_po_fk3` FOREIGN KEY (`payment_option_mst_id`) REFERENCES `payment_option_mst` (`payment_option_mst_id`),
  CONSTRAINT `server_pricing_tmp_sa_fk4` FOREIGN KEY (`server_attributes_id`)  REFERENCES `server_attributes_tmp` (`server_attributes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;