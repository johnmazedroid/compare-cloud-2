from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import time
import os
from datetime import datetime
import shutil
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as ec

def make_screenshot():
 try:
#To run without browser visible
  options =  Options()
  options.add_argument("--window-size=%s" %"1920,1080")
  options.add_argument('--headless')
  options.add_argument('--disable-gpu')  # Last I checked this was necessary.

#windows parameters
#DRIVER_PATH = 'C:\\temp\\today\\python\\chrome_driver_85\\chromedriver.exe'
#driver = webdriver.Chrome(executable_path=DRIVER_PATH,options=options)
#unix parameters
  DRIVER_PATH = '/home/schumideena/python/chromedriver'
  driver = webdriver.Chrome('/home/schumideena/python/chromedriver85',chrome_options=options)

  driver.get("https://cloudpricingcalculator.appspot.com/")
#driver.get('https://cloud.google.com/products/calculator')
#  driver.implicitly_wait(30) # seconds

  wait = WebDriverWait(driver, 2)

#Output file property declaration
  now = str(datetime.now().strftime("%Y%m%d-%H%M%S"))

  srcpath = os.path.dirname(__file__)
  backuppath = os.path.join(os.path.dirname(__file__),'archive',now)
  srcfile=os.path.join(srcpath,"output.txt")




  if os.path.isfile(srcfile):
    #create archive directory if doesnt exist
    os.makedirs(os.path.dirname(backuppath), exist_ok=True)
    shutil.copyfile(srcfile,backuppath)

#Creating a output file before uploading to DB

  outputurls2file = open(srcfile, "w")

#no_instances = driver.find_element_by_id('input_60')
  no_instances = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Number of instances")]/following-sibling::input')


    #enter the instances
  no_instances.send_keys('1') 


#select the input box of OS/Software selection

#os_input = driver.find_element_by_xpath('//*[@id="select_value_label_53"]/span[1]/div')
  os_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Operating System / Software")]/following-sibling::md-select/md-select-value')


#clicking to enable the select options
  if os_input.is_displayed(): 
    os_input.click()

#Gather the dropdown options
#os_dropdown = driver.find_element_by_id('select_container_74')
  os_dropdown_id = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Operating System / Software")]/following-sibling::md-select').get_attribute('aria-owns')
  os_dropdown = driver.find_element_by_id(os_dropdown_id)
  os_options = os_dropdown.find_elements_by_tag_name('md-option')
#Loop through the options in the DropDown
#print('OS Options\n')
#print('----------------\n')
  for os_option in os_options:
    #print (os_option.get_attribute('value'))
    #os_value=os_option.get_attribute('value')
    print(os_option.get_attribute("id"))
    #if os_option.get_attribute('value') == 'win' :
    os_option.click()
    os_value = str(driver.find_element_by_xpath('//*[@id="'+os_option.get_attribute("id")+'"]/div').text)    

    #select the input box of Machine Class
    #class_input = driver.find_element_by_xpath('//*[@id="select_value_label_54"]/span[1]/div')
    class_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Class")]/following-sibling::md-select/md-select-value/span[1]/div')
    #Enable the dropdown options
    if class_input.is_displayed():
        class_input.click()

    #Gather the dropdown options
    class_dropdown_id = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Class")]/following-sibling::md-select').get_attribute('aria-owns')
    #class_dropdown = driver.find_element_by_id('select_container_78')
    class_dropdown = driver.find_element_by_id(class_dropdown_id)
    class_options = class_dropdown.find_elements_by_tag_name('md-option')
    #print('\nMachine Class\n')
    #print('----------------\n')
    for class_option in class_options:
        #print(class_option.get_attribute('value'))
        class_value=class_option.get_attribute('value')
        #if class_option.get_attribute('value') == 'preemptible' :
        if class_option.is_enabled():
            class_option.click()
        
        #Select the input box of Machine family
        #family_input = driver.find_element_by_xpath('//*[@id="select_value_label_55"]/span[1]/div') 
        family_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Family")]/following-sibling::md-select/md-select-value/span[1]/div')
        #Enable the dropdown options    
        family_input.click()

        #Gather the dropdown options
        #family_dropdown = driver.find_element_by_id('select_container_83')
        family_dropdown_id = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Family")]/following-sibling::md-select').get_attribute('aria-owns')
        family_dropdown = driver.find_element_by_id(family_dropdown_id)
        family_options = family_dropdown.find_elements_by_tag_name('md-option')

        #print('\nMachine Family\n')
        #print('-------------------')

        for family_option in family_options:
            
            #print(family_option.get_attribute('value'))
            family_value=family_option.get_attribute('value')
            family_option.click()
            
            
            #Select the input box of Machine series
            #series_input = driver.find_element_by_xpath('//*[@id="select_value_label_56"]/span[1]/div') 
            series_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Series")]/following-sibling::md-select/md-select-value/span[1]/div')
            #Enable the dropdown option
            series_input.click()


            #Gather the dropdown options
            #series_dropdown = driver.find_element_by_id('select_container_85')
            series_dropdown_id =  driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Series")]/following-sibling::md-select').get_attribute('aria-owns')
            series_dropdown = driver.find_element_by_id(series_dropdown_id)
            series_options = series_dropdown.find_elements_by_tag_name('md-option')
            
            #print('\nMachine Series\n')
            #print('-------------------')

            for series_option in series_options :
                #print(series_option.get_attribute('value'))
                series_value=series_option.get_attribute('value')
                series_option.click()

                #Select the input box of Machine Type
                type_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine type")]/following-sibling::md-select/md-select-value/span[1]/div')
                
                #type_input = driver.find_element_by_xpath('//*[@id="select_value_label_57"]/span[1]/div') 
                #Enable the dropdown option
                #wait.until(ec.element_to_be_clickable((By.ID,type_option.get_attribute('id')))).click()
                type_input.click()


                #Gather the dropdown options
                type_dropdown_id = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine type")]/following-sibling::md-select').get_attribute('aria-owns')
                #type_dropdown = driver.find_element_by_id('select_container_87')
                type_dropdown = driver.find_element_by_id(type_dropdown_id)
                type_options = type_dropdown.find_elements_by_tag_name('md-option')
            
                #print('\nMachine Type\n')
                #print('-------------------')

                for type_option in type_options :
                    #print(type_option.get_attribute('value')+type_option.get_attribute('id'))
                    type_value = type_option.get_attribute('value')
                    if type_option.get_attribute('value') != 'custom' :
                        #wait.until(ec.presence_of_element_located((By.ID,type_option.get_attribute('id')))).click()
                        #type_option.click()
                        ActionChains(driver).move_to_element(type_option).click(type_option).perform()
                        vcpu_value = str(type_option.text).split('(')[-1].split(')')[0].split(',')[0]
                        ram_value = str(type_option.text).split('(')[-1].split(')')[0].split(',')[-1]
                       # print(str(type_option.text))
                    else:
                        continue
                        
                    #Select the input box of SSD Local Disk
                    ssd_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Local SSD")]/following-sibling::md-select/md-select-value/span[1]/div')
                    #ssd_input = driver.find_element_by_xpath('//*[@id="select_value_label_192"]/span[1]/div') 
                    #Enable the dropdown option
                    ssd_input.click()
                    
                    
                    #Gather the dropdown options
                    ssd_dropdown_id = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Local SSD")]/following-sibling::md-select').get_attribute('aria-owns')
                    #ssd_dropdown = driver.find_element_by_id('select_container_194')
                    ssd_dropdown = driver.find_element_by_id(ssd_dropdown_id)
                    ssd_options = ssd_dropdown.find_elements_by_tag_name('md-option')



                   # print('\nSSD Local Disk\n')
                    #print('-------------------')

                    for ssd_option in ssd_options :
                     #   print(ssd_option.get_attribute('value'))
                        ssd_value = ssd_option.get_attribute('value')
                        if ssd_input.is_displayed():
                            wait.until(ec.element_to_be_clickable((By.ID,ssd_option.get_attribute('id')))).click()
                            #ssd_option.click()
                        
                        #Select the input box of Region
                        #region_input = driver.find_element_by_xpath('//*[@id="select_value_label_58"]/span[1]/div') 
                        region_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Datacenter location")]/following-sibling::md-select/md-select-value/span[1]/div')
                        #Enable the dropdown option
                        region_input.click()


                        #Gather the dropdown options
                        region_dropdown_id = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Datacenter location")]/following-sibling::md-select').get_attribute('aria-owns')
                        #region_dropdown = driver.find_element_by_id('select_container_89')
                        region_dropdown = driver.find_element_by_id(region_dropdown_id)
                        region_options = region_dropdown.find_elements_by_tag_name('md-option')

                        
                        #print('\nRegion\n')
                        #print('-------------------')

                        for region_option in region_options :
                           # print(region_option.get_attribute('value'))
                            region_value=region_option.get_attribute('value')
                            #if region_option.get_attribute('value') == 'us-central1':
                            if region_input.is_enabled():
                                #action = ActionChains(driver).move_to_element(region_option).click(region_option).perform()
                                ActionChains(driver).move_to_element(region_option).click(region_option).perform()
                               # action.move_to_element(region_option)
                                #region_option.click()
                                #action.click(region_option).perform()
                                #action.perform()
                                
                            #Click the Estimate
                            button_input = driver.find_element_by_xpath('//*[@id="mainForm"]/div[2]/div/md-card/md-card-content/div/div[1]/form/*/button')
                            button_input.click()
       
                            #Get the hourly Rate 
                            #hrly_rate = driver.find_element_by_xpath('//*[@id="compute"]/md-list/md-list-item[6]/div')
                            hrly_rate = driver.find_element_by_xpath('//*[@id="compute"]/md-list/*/div[contains(normalize-space(), "Effective Hourly Rate")]')
                            
                            print(os_value+' | '+class_value +' | '+ family_value+' | '+ series_value +' | '+ type_value +' | '+ ssd_value +' X 375 GB |'+ vcpu_value +' | '+ ram_value +' | '+region_value + ' |' +hrly_rate.text.split(" ")[-1])   
                            
                            outputurls2file.write(os_value+' | '+class_value +' | '+ family_value+' | '+ series_value +' | '+ type_value +' | '+ ssd_value +' |'+ vcpu_value +' | '+ ram_value +' | '+region_value + ' |' +hrly_rate.text.split(" ")[-1])
                            outputurls2file.write("\n")
                            
                            time.sleep(1)
                            
                            delete_button= driver.find_element_by_xpath('//*[@id="compute"]/md-list/div/div[2]/button[2]')
                            #action = ActionChains(driver)
                            #action.move_to_element(delete_button)
                            #delete_button.click()
                            delete_button.click()
                            
                            
                            
                            #no_instances = driver.find_element_by_id('input_60')
                            no_instances = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Number of instances")]/following-sibling::input')


                            #enter the instances
                            no_instances.send_keys('1') 
                        
                            if region_option != region_options[-1]:
                                #region_input = driver.find_element_by_xpath('//*[@id="select_value_label_58"]/span[1]/div') 
                                #region_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Datacenter location")]/following-sibling::md-select/md-select-value/span[1]/div')
                                region_input = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Datacenter location")]/following-sibling::md-select/md-select-value/span[1]/div')))
                                #Enable the dropdown option
                                if region_input.is_displayed():
                                    region_input.click()
                            
                            
                        
                        #skip for the last option
                        if ssd_option != ssd_options[-1]:
                            #Select the input box of SSD Local Disk
                            #ssd_input = driver.find_element_by_xpath('//*[@id="select_value_label_192"]/span[1]/div') 
                            #ssd_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Local SSD")]/following-sibling::md-select/md-select-value/span[1]/div')
                            ssd_input = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Local SSD")]/following-sibling::md-select/md-select-value/span[1]/div'))) 
                            #Enable the dropdown option
                            if ssd_input.is_displayed():
                                ssd_input.click()

                        
                        
                    #skip for the last option 
                    if type_option != type_options[-1]:
                        #Select the input box of Machine Type
                        #type_input = driver.find_element_by_xpath('//*[@id="select_value_label_58"]/span[1]/div') 
                        #type_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine type")]/following-sibling::md-select/md-select-value/span[1]/div')
                        type_input = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Machine type")]/following-sibling::md-select/md-select-value/span[1]/div'))) 
                        if type_input.is_enabled():
                            type_input.click()
                
                #skip for the last option                
                if series_option != series_options[-1] :
                    #Select the input box of Machine series
                    #series_input = driver.find_element_by_xpath('//*[@id="select_value_label_57"]/span[1]/div') 
                    #series_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Series")]/following-sibling::md-select/md-select-value/span[1]/div')
                    series_input = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Series")]/following-sibling::md-select/md-select-value/span[1]/div')))
                    #Enable the dropdown option
                    series_input.click()
            
            #skip for the last option
            if family_option != family_options[-1] :
                #Select the input box of Machine family
                #family_input = driver.find_element_by_xpath('//*[@id="select_value_label_56"]/span[1]/div') 
                #family_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Family")]/following-sibling::md-select/md-select-value/span[1]/div')
                family_input = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Machine Family")]/following-sibling::md-select/md-select-value/span[1]/div')))
                #Enable the dropdown options    
                if family_input.is_enabled() :
                    family_input.click()
            
        #skip for the last option
        if class_option != class_options[-1] :
            #select the input box of Machine Class
            #family_input_div = driver.find_element_by_xpath('//*[@id="select_value_label_56"]/span[1]/div')
            #family_input_div = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Family")]/following-sibling::md-select/md-select-value/span[1]/div')
            family_input_div = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Machine Family")]/following-sibling::md-select/md-select-value/span[1]/div')))
            family_input_div.click()
            
            #Selecting General Purpose
            driver.find_element_by_id(driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Family")]/following-sibling::md-select').get_attribute('aria-owns')).find_elements_by_tag_name('md-option')[0].click()
            #driver.find_element_by_id('select_container_83').find_elements_by_tag_name('md-option')[0].click()
         
            #class_input = driver.find_element_by_xpath('//*[@id="select_value_label_55"]/span[1]/div')
            #class_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Machine Class")]/following-sibling::md-select/md-select-value/span[1]/div')
            class_input = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Machine Class")]/following-sibling::md-select/md-select-value/span[1]/div')))
            if class_input.is_enabled():
                class_input.click()
    
      
    
    if os_option != os_options[-1]:
       #os_input = driver.find_element_by_xpath('//*[@id="select_value_label_54"]/span[1]/div')
       #os_input = driver.find_element_by_xpath('//*/label[contains(normalize-space(), "Operating System / Software")]/following-sibling::md-select/md-select-value')
       os_input = wait.until(ec.element_to_be_clickable((By.XPATH,'//*/label[contains(normalize-space(), "Operating System / Software")]/following-sibling::md-select/md-select-value')))
       #Activate the OS input box
       os_input.click()            

  outputurls2file.close()
    
 except:
  driver.save_screenshot('screenshot_'+now+'.png')
  driver.close()
  raise

if __name__ == "__main__":
    make_screenshot()



        
  


